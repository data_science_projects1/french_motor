import os
import sys
from pathlib import Path

path = Path(os.path.dirname(os.path.realpath(__file__)))
parent = str(path.parent.parent.absolute()) + '/'
sys.path.append(parent)
