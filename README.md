# French Motor Insurance Analysis

Repository is loading and analyzing Motor insurance data from OpenML.

Data was downloaded from

Frequency: [https://www.openml.org/search?type=data&sort=runs&id=41214&status=active]

Severity: [https://www.openml.org/search?type=data&sort=runs&id=41215&status=active]

and saved in folder `data`.

A virtual environment with `pipenv` is created with the packages needed for analysis.

The folder `scripts` holds the python files.


**Step 1**: Load data


**Step 2**: Folder `scripts/data` contains valid feature values for existing columns and
holds classes for dataset description and preprocessing/loading data.


**Step 3**: Setup `schema_severity.py` and `schema_frequency.py` in `scripts/schema` to validate loaded data against expectations.


**Step 4**: Preprocess data to match schemas and prepare modeling
* Frequency
    * Remove trailing/leading sign
    * Cast cols to integer
    * Clip exposure weight, vehicle age, claim number
    * Add new feature young driver flag
    * Add interaction for vehicle power x vehicle gas
    * Add interaction for vehicle power x young driver flag

* Severity
    * Cast cols to integer
    * Group claims by policy id
    * Clip claim amount beyond large claim threshold and reallocate the trimmed amount
    to the remaining claims


**Step 5**: Prepare Modeling data

* Merge frequency data with severity data with left join
* Fillna claim amount with `0`
* Reset claim number for zero claims to `0`
* Add target value `burning cost = claim amount / exposure`
* Add target value `frequency = claim number / exposure`
* Add target value `average claim amount = claim amount / claim numbers`
* Group `density` in buckets
* Group `bonus_malus` in buckets
* Encode categorical columns


**Step 6**: Run `plausibility_checks.py`

* Check that:
    * Zero claims are removed
    * No rows with claim amount but zero claim number
    * No rows with zero exposure
    * Claim amount clipping threshold is not set to low
    * Exposure clipping threshold is not set to low
    * Claim number clipping threshold is not set to low
    * There are no missing values
    * Every claim can be joined via policy_id


**Step 7**: Analysis

* Jupyter Notebooks are located in `notebooks` folder.
* They calls the `main.py` file from `scripts`. A pandas dataframe `df_modeling` is returned back for analysis.
* The `eda` notebook:
    * boxplots
    * per feature the observed burning cost, claim frequency and avg claim size.
    * Section `interaction` shows the reason why the 2 interactions are added.
    * correlation matrix for selected features is plotted.
* `regionalization` notebook:
    * Plot regional information in a map of France
* Subfolder `ensemble` contains:
    * Notebook `0_model_comparison`
        * Ensemble methods
            * Gradient Boosting Regressor (GBR)
            * Adaboost Regressor
            * Bagging Regressor
            * Xgboost
        * Double check best ensemble method with KFold algorithm
    * Notebook `1_gbr`
        * Finetune hyperparameters for GBR by `RandomizedSearchCV`
        * Show feature importance for optimised GBR
        * Plot observed vs predicted for train and test data
        * Analysis results whith `SHAP` values
    * Notebook `2_catboost_regressor`
      * Finetune model parameters
      * Plot observed vs predicted for test and train group
* Subfolder `glm` contains:
    * Training of
        * severity model
        * frequency model
        * Tweedie model
    * Models are tracked to **MLFlow** for model versioning


**Results**
* The fine-tuned Tweedie with feature engineering performs similar to a GBM model regarding the `MAE/RMSE` metric.
* The Tweedie deviance is not comparable between optimized and non-optimized model because the tweedie parameter in optimized model is lower with results in higher weights on extreme deviations between target and predicted.


**Next steps**
* Model stacking: Build a further model training the residuals


# Development local
## Setup

*   Run
```bash
  make install
```
or
```bash
    pip install pipenv
    pipenv install --dev
    pipenv run pre-commit install
```

## Code QA

*  Checking if code changes will block a commit by
```bash
    make pre-commit
```
or
```bash
    pipenv run pre-commit run --all-files
```


# Tests
Run
```bash
  make pytest
```
or
```bash
pipenv run pytest test
```


# Development in Docker
To initially build run
```bash
    docker-compose up --build
```
or
```bash
    make build
```

For already build images use

```bash
    make start
```
or

```bash
    docker-compose up
```

It starts **MLFlow** server (http://localhost:5000) and **jupyter-lab** server (link in docker logs).
