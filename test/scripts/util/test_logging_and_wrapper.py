import logging
from typing import Any

import pytest

from scripts.util import DataScienceFrame, log_df


@pytest.fixture
def df_test() -> DataScienceFrame:
    return DataScienceFrame(data={'col1': ['abcd'], 'col2': [1234]})


def test_log_df(df_test: DataScienceFrame, caplog: Any) -> None:
    @log_df
    def decorated_func() -> DataScienceFrame:
        return df_test

    with caplog.at_level(logging.INFO):
        decorated_func()

    assert 'function: decorated_func, df shape: (1, 2)' in caplog.text
