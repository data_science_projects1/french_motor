from scripts.util import DataScienceFrame


def test_remove_char_from_cols() -> None:
    test_data = {
        'area': ["'A'", "'B'", 'C', 'D', "'E'"],
        'region': ["'A'", "'B'", 'C', 'D', "'E'"],
        'expected_area': ['A', 'B', 'C', 'D', 'E'],
    }
    df_test = DataScienceFrame(data=test_data)
    df_transformed_one_col = df_test.remove_char_from_cols(cols='area', char="'")
    df_transformed_two_cols = df_test.remove_char_from_cols(cols=['area', 'region'], char="'")
    assert df_transformed_one_col['area'].equals(df_test.expected_area)
    assert df_transformed_two_cols['area'].equals(df_test.expected_area)
    assert df_transformed_two_cols['region'].equals(df_test.expected_area)
