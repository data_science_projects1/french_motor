import pytest

from scripts.models.glm.tweedie.tweedie_model import TweedieEncoders
from test import assert_transformed_values


@pytest.mark.parametrize(
    argnames='input,expected',
    argvalues=[
        ('A', [1, 0, 0]),
        ('B', [0, 0, 0]),
        ('C', [1, 0, 0]),
        ('D', [0, 1, 0]),
        ('E', [1, 0, 0]),
        ('F', [0, 0, 1]),
    ],
)
def test_area(input: str, expected: list[int]) -> None:
    area = TweedieEncoders.area
    assert_transformed_values(transformation=area, input=input, expected=expected)


@pytest.mark.parametrize(
    argnames='input,expected',
    argvalues=[
        (100, [1, 0, 0, 0, 0]),
        (200, [1, 1, 0, 0, 0]),
        (400, [1, 2, 0, 0, 0]),
        (500, [1, 3, 0, 0, 0]),
        (800, [1, 6, 1, 0, 0]),
        (1000, [1, 6, 0, 1, 0]),
        (1400, [1, 6, 0, 0, 0]),
        (2000, [1, 6, 0, 0, 0]),
        (3400, [1, 6, 0, 0, 1]),
        (5400, [1, 6, 0, 0, 1]),
    ],
)
def test_density_grouped(input: str, expected: list[int]) -> None:
    density = TweedieEncoders.density
    assert_transformed_values(transformation=density, input=input, expected=expected)
