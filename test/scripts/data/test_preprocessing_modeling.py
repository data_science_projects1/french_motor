import pytest
from pandas.testing import assert_frame_equal

from scripts.data import Frequency, Modeling, Severity
from scripts.util import DataScienceFrame


@pytest.fixture
def df_test() -> DataScienceFrame:
    test_data = {
        Severity.claim_amount: [0, 100],
        Frequency.claim_nb: [1, 1],
        Modeling.exposure: [1, 1],
        'expected_claim_nb_adjust': [0, 1],
        'expected_bc': [0, 100.0],
        'expected_frequency': [1.0, 1.0],
        'expected_avg_clm_amt': [0.0, 100],
    }
    return DataScienceFrame(data=test_data)


def test_adjust_zero_claims(df_test: DataScienceFrame) -> None:
    assert Modeling._adjust_zero_claims(df_test)[Frequency.claim_nb].equals(df_test.expected_claim_nb_adjust)


def test_add_burning_cost(df_test: DataScienceFrame) -> None:
    assert Modeling._add_burning_cost(df_test)[Modeling.burning_cost].equals(df_test.expected_bc)


def test_add_frequency(df_test: DataScienceFrame) -> None:
    assert Modeling._add_frequency(df_test)[Modeling.frequency].equals(df_test.expected_frequency)


def test_add_avg_claim_amount(df_test: DataScienceFrame) -> None:
    assert Modeling._add_avg_claim_amount(df_test)[Modeling.avg_claim_amount].equals(df_test.expected_avg_clm_amt)


def test_remove_claims_with_no_frequency() -> None:
    df_severity = DataScienceFrame(
        data={
            Severity.id_pol: ['1', '1', '2'],
            'sec_col': [1, 2, 3],
        },
    )
    df_frequency = DataScienceFrame(data={Frequency.id_pol: ['1', '1']})

    df_observed = Modeling._remove_claims_with_no_frequency(df_frequency=df_frequency, df_severity=df_severity)
    df_expected = DataScienceFrame(
        data={
            Severity.id_pol: ['1', '1'],
            'sec_col': [1, 2],
        },
    )

    assert_frame_equal(left=df_expected, right=df_observed)
