import pytest
from pandas.testing import assert_frame_equal

from scripts.data import Frequency, FuelType, Modeling, ModelingFeatureEngineering
from scripts.util import DataScienceFrame


@pytest.fixture
def df_test() -> DataScienceFrame:
    data = {
        Frequency.density: [310, 50, 60, 27_001, 9700],
        'expected_density': [300, 0, 100, 27_000, 9300],
        Frequency.bonus_malus: [45, 50, 51, 58, 112],
        'expected_bonus_malus': [0, 50, 51, 57, 100],
        Modeling.bonus_malus_grouped: [0, 50, 100, 200, 300],
        'expected_bonus_malus_binarizer': [1, 1, 0, 0, 0],
        'expected_bonus_malus_discretizer': [0, 1, 2, 2, 2],
        Modeling.driv_age: [18, 19, 20, 40, 50],
        'expected_yd_flag': [1, 1, 1, 0, 0],
        Modeling.veh_power: [5, 6, 7, 8, 9],
        Modeling.veh_gas: [
            FuelType.diesel,
            FuelType.regular,
            FuelType.regular,
            FuelType.diesel,
            FuelType.diesel,
        ],
        'expected_ia_fuel_power': [
            'veh_power_l7_x_diesel',
            'veh_power_l7_x_regular',
            'rest',
            'rest',
            'rest',
        ],
        Modeling.young_driver_flag: [1, 1, 0, 0, 0],
        'expected_ia_power_yd_flag': [5, 6, 7, 0, 0],
        'expected_encoded_veh_gas': [0, 1, 1, 0, 0],
    }
    return DataScienceFrame(data=data)


@pytest.fixture
def model_preprocessor(df_test: DataScienceFrame) -> ModelingFeatureEngineering:
    return ModelingFeatureEngineering(df_test)


def test_grouping_density(df_test: DataScienceFrame, model_preprocessor: ModelingFeatureEngineering) -> None:
    assert model_preprocessor._grouping_density()[Modeling.density_grouped].equals(df_test.expected_density)


def test_bonus_malus(df_test: DataScienceFrame, model_preprocessor: ModelingFeatureEngineering) -> None:
    assert model_preprocessor._grouping_bonus_malus()[Modeling.bonus_malus_grouped].equals(df_test.expected_bonus_malus)


def test_add_young_driver_flag(df_test: DataScienceFrame, model_preprocessor: ModelingFeatureEngineering) -> None:
    assert model_preprocessor._add_young_driver_flag()[Modeling.young_driver_flag].equals(df_test.expected_yd_flag)


def test_add_interaction_veh_power_x_veh_gas(
    df_test: DataScienceFrame,
    model_preprocessor: ModelingFeatureEngineering,
) -> None:
    assert model_preprocessor._add_interaction_veh_power_x_veh_gas()[Modeling.veh_power_x_veh_gas].equals(
        df_test.expected_ia_fuel_power,
    )


def test_add_interaction_veh_power_x_young_driver_flag(
    df_test: DataScienceFrame,
    model_preprocessor: ModelingFeatureEngineering,
) -> None:
    df_observed = model_preprocessor._add_young_driver_flag()
    df_observed = model_preprocessor._add_interaction_veh_power_x_young_driver_flag()
    assert df_observed[Modeling.veh_power_x_young_driver_flag].equals(
        df_test.expected_ia_power_yd_flag,
    )


def test_encode_veh_gas(df_test: DataScienceFrame, model_preprocessor: ModelingFeatureEngineering) -> None:
    assert model_preprocessor._encode_veh_gas()[Modeling.veh_gas_encoded].equals(
        df_test.expected_encoded_veh_gas,
    )


def test_encode_categorical_cols() -> None:
    data = {
        'VehBrand': ['B1', 'B2'],
        'Area': ['A', 'B'],
        'Region': ['R1', 'R2'],
        'veh_power_x_veh_gas': ['veh_power_l7_x_diesel', 'rest'],
    }
    df_input = DataScienceFrame(data=data)

    df_expected = df_input.join(
        DataScienceFrame(
            data={
                'VehBrand_B1': [1.0, 0.0],
                'VehBrand_B2': [0.0, 1.0],
                'Area_A': [1.0, 0.0],
                'Area_B': [0.0, 1.0],
                'Region_R1': [1.0, 0.0],
                'Region_R2': [0.0, 1.0],
                'veh_power_x_veh_gas_rest': [0.0, 1.0],
                'veh_power_x_veh_gas_veh_power_l7_x_diesel': [1.0, 0.0],
            },
        ),
    )
    df_observed = ModelingFeatureEngineering(df_input)._encode_categorical_cols()

    assert_frame_equal(left=df_expected, right=df_observed)
