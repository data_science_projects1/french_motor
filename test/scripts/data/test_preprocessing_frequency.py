import pytest

from scripts.data import Frequency
from scripts.util import DataScienceFrame


@pytest.fixture
def df_test() -> DataScienceFrame:
    test_data = {
        Frequency.exposure: [0, 0.5, 1, 1.5, 2],
        'expected_exposure': [0, 0.5, 1, 1.0, 1.0],
        Frequency.claim_nb: [0, 1, 2, 4, 5],
        'expected_claim_nb': [0, 1, 2, 3, 3],
        Frequency.veh_age: [0, 10, 20, 40, 50],
        'expected_veh_age': [0, 10, 20, 30, 30],
        Frequency.area: ["'A'", "'B'", 'C', 'D', "'E'"],
        'expected_area': ['A', 'B', 'C', 'D', 'E'],
    }
    return DataScienceFrame(data=test_data)


def test_kupierung_weight(df_test: DataScienceFrame) -> None:
    df_transformed = Frequency._kupierung(
        df_frequency=df_test,
        col=Frequency.exposure,
        threshold=Frequency.KUPIERUNG_WEIGHT,
    )
    assert df_transformed[Frequency.exposure].equals(df_test.expected_exposure)


def test_kupierung_claim_nb(df_test: DataScienceFrame) -> None:
    df_transformed = Frequency._kupierung(
        df_frequency=df_test,
        col=Frequency.claim_nb,
        threshold=Frequency.KUPIERUNG_CLAIM_NB,
    )
    assert df_transformed[Frequency.claim_nb].equals(df_test.expected_claim_nb)


def test_kupierung_vehicle_age(df_test: DataScienceFrame) -> None:
    df_transformed = Frequency._kupierung(
        df_frequency=df_test,
        col=Frequency.veh_age,
        threshold=Frequency.VEHICLE_AGE_LIMIT,
    )
    assert df_transformed[Frequency.veh_age].equals(df_test.expected_veh_age)
