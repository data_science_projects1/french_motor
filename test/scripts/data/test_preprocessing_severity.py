import pandas as pd
import pytest

from scripts.data import Severity
from scripts.util import DataScienceFrame


@pytest.fixture
def df_grouping_input() -> DataScienceFrame:
    test_data = {
        Severity.claim_amount: [1000, 2000, 100, 55_000],
        Severity.id_pol: [1, 1, 2, 3],
    }
    return DataScienceFrame(data=test_data)


@pytest.fixture
def df_grouping_output() -> DataScienceFrame:
    test_data = {Severity.id_pol: [1, 2, 3], Severity.claim_amount: [3000, 100, 55_000]}
    return DataScienceFrame(data=test_data)


@pytest.fixture
def df_kupierung_input() -> DataScienceFrame:
    test_data = {Severity.claim_amount: [3000.0, 100.0, 55_000.0], Severity.id_pol: [1, 2, 3]}
    return DataScienceFrame(data=test_data)


@pytest.fixture
def df_kupierung_output() -> DataScienceFrame:
    test_data = {
        Severity.claim_amount: [7838.709677, 261.290323, 50_000],
        Severity.id_pol: [1, 2, 3],
        Severity.claim_amount_capped: [0.0, 0.0, 5_000],
    }
    return DataScienceFrame(data=test_data)


def test_group_claims_by_policy_id(
    df_grouping_input: DataScienceFrame,
    df_grouping_output: DataScienceFrame,
) -> None:
    assert Severity._group_claims_by_policy_id(df_grouping_input).equals(df_grouping_output)


def test_kupierung_large_claims_and_reallocate_truncated_claim_amount(
    df_kupierung_input: DataScienceFrame,
    df_kupierung_output: DataScienceFrame,
) -> None:
    pd.testing.assert_frame_equal(
        Severity._kupierung_large_claims_and_reallocate_truncated_claim_amount(df_kupierung_input),
        df_kupierung_output,
        check_exact=False,
    )
