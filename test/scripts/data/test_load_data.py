import pytest

from scripts.data import Frequency, Severity
from scripts.util import DataScienceFrame


@pytest.fixture
def df_freq_loaded() -> DataScienceFrame:
    return Frequency._load_data()


@pytest.fixture
def df_sev_loaded() -> DataScienceFrame:
    return Severity._load_data()


def test_df_freq_loaded_dtype(df_freq_loaded: DataScienceFrame) -> None:
    assert isinstance(df_freq_loaded, DataScienceFrame)


def test_df_sev_loaded_dtype(df_sev_loaded: DataScienceFrame) -> None:
    assert isinstance(df_sev_loaded, DataScienceFrame)


def test_df_freq_loaded_cols(df_freq_loaded: DataScienceFrame) -> None:
    assert df_freq_loaded.columns.to_list() == Frequency._get_file_col_names()


def test_df_sev_loaded_cols(df_sev_loaded: DataScienceFrame) -> None:
    assert df_sev_loaded.columns.to_list() == Severity._get_file_col_names()
