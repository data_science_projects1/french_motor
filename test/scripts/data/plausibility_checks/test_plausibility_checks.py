import pytest

from scripts.data.plausibility_checks.plausibility_checks import (
    _check_claim_zero_but_claim_amount_not,
    _check_for_missing_values,
    _check_for_zero_claims,
    _check_for_zero_exposure,
)
from scripts.util import DataScienceFrame


def test_check_claim_zero_but_claim_amount_not() -> None:
    df_input = DataScienceFrame(data={'ClaimNb': [0], 'ClaimAmount': [1000]})

    with pytest.raises(ValueError):
        _check_claim_zero_but_claim_amount_not(df_input)


def test_check_for_zero_claims() -> None:
    df_input = DataScienceFrame(data={'ClaimNb': [2], 'ClaimAmount': [0]})

    with pytest.raises(ValueError):
        _check_for_zero_claims(df_input)


def test_check_for_zero_exposure() -> None:
    df_input = DataScienceFrame(data={'Exposure': [0]})

    with pytest.raises(ValueError):
        _check_for_zero_exposure(df_input)


def test_check_for_missing_values() -> None:
    df_input = DataScienceFrame(data={'Exposure': [None], 'tt': [None]})

    with pytest.raises(ValueError):
        _check_for_missing_values(df_input)
