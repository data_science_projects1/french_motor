import numpy as np
import pytest

from scripts.encoding.rounder import Rounder


@pytest.fixture
def rounder_instance() -> Rounder:
    return Rounder(buckets=10)


def test_rounder_fit(rounder_instance: Rounder) -> None:
    assert rounder_instance.fit() is rounder_instance


def test_rounder_transform_basic(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([5, 15, 25, 35])
    expected_output: np.ndarray = np.array([0, 20, 20, 40])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_negative_values(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([-5, -15, -25, -35])
    expected_output: np.ndarray = np.array([0, -20, -20, -40])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_mixed_values(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([5, -15, 25, -35])
    expected_output: np.ndarray = np.array([0, -20, 20, -40])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_floats(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([5.7, 14.3, 25.9, 35.1])
    expected_output: np.ndarray = np.array([10, 10, 30, 40])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_empty_array(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([])
    expected_output: np.ndarray = np.array([])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_large_buckets() -> None:
    rounder = Rounder(buckets=50)
    input_array: np.ndarray = np.array([45, 55, 125, 175])
    expected_output: np.ndarray = np.array([50, 50, 100, 200])
    output: np.ndarray = rounder.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_single_element(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([47])
    expected_output: np.ndarray = np.array([50])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)


def test_rounder_transform_edge_case_values(rounder_instance: Rounder) -> None:
    input_array: np.ndarray = np.array([0, 10, 20, -10, -20])
    expected_output: np.ndarray = np.array([0, 10, 20, -10, -20])
    output: np.ndarray = rounder_instance.transform(input_array)
    np.testing.assert_array_equal(output, expected_output)
