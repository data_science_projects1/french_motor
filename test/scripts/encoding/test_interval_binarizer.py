import numpy as np
import pytest

from scripts.encoding.interval_binarizer import IntervalBinarizer


def test_interval_binarizer_transform_within_threshold() -> None:
    binarizer = IntervalBinarizer(lower_threshold=5, upper_threshold=10)
    input_array = np.array([4, 5, 6, 9, 10, 11])
    transformed = binarizer.transform(input_array)

    expected_result = np.array([0, 1, 1, 1, 0, 0])
    np.testing.assert_array_equal(transformed, expected_result)


def test_interval_binarizer_transform_below_threshold() -> None:
    binarizer = IntervalBinarizer(lower_threshold=5, upper_threshold=10)
    input_array = np.array([-10, -5, 0, 3])
    transformed = binarizer.transform(input_array)

    expected_result = np.array([0, 0, 0, 0])
    np.testing.assert_array_equal(transformed, expected_result)


def test_interval_binarizer_transform_above_threshold() -> None:
    binarizer = IntervalBinarizer(lower_threshold=5, upper_threshold=10)
    input_array = np.array([10, 11, 15, 20])
    transformed = binarizer.transform(input_array)

    expected_result = np.array([0, 0, 0, 0])
    np.testing.assert_array_equal(transformed, expected_result)


def test_interval_binarizer_invalid_thresholds() -> None:
    with pytest.raises(ValueError, match='upper_threshold should be greater than lower_threshold'):
        IntervalBinarizer(lower_threshold=10, upper_threshold=5)


def test_interval_binarizer_edge_values() -> None:
    binarizer = IntervalBinarizer(lower_threshold=5, upper_threshold=10)
    input_array = np.array([5, 9, 10])
    transformed = binarizer.transform(input_array)

    expected_result = np.array([1, 1, 0])
    np.testing.assert_array_equal(transformed, expected_result)


def test_interval_binarizer_single_value_array() -> None:
    binarizer = IntervalBinarizer(lower_threshold=5, upper_threshold=10)
    input_array = np.array([7])
    transformed = binarizer.transform(input_array)

    expected_result = np.array([1])
    np.testing.assert_array_equal(transformed, expected_result)
