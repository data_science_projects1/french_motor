import numpy as np
import pytest

from scripts.encoding.bucket_creator import BucketCreator


def test_bucket_creator_init() -> None:
    buckets = np.array([10, 20, 30, 40])
    bucket_creator = BucketCreator(buckets)
    assert np.array_equal(bucket_creator.buckets, buckets)


def test_bucket_creator_fit() -> None:
    buckets = np.array([10, 20, 30, 40])
    bucket_creator = BucketCreator(buckets)
    assert bucket_creator.fit() == bucket_creator


@pytest.mark.parametrize(
    'buckets, data, expected',
    [
        (np.array([10, 20, 30, 40]), np.array([35, 45, 25, 10]), np.array([30, 40, 20, 10])),
        (np.array([5, 15, 25, 35]), np.array([7, 22, 33]), np.array([5, 15, 25])),
    ],
)
def test_bucket_creator_transform(buckets: np.ndarray, data: np.ndarray, expected: np.ndarray) -> None:
    bucket_creator = BucketCreator(buckets)
    transformed = bucket_creator.transform(data)
    assert np.array_equal(transformed, expected)


def test_bucket_creator_transform_empty() -> None:
    buckets = np.array([10, 20, 30])
    data = np.array([])
    expected = np.array([])
    bucket_creator = BucketCreator(buckets)
    transformed = bucket_creator.transform(data)
    assert np.array_equal(transformed, expected)


@pytest.mark.parametrize(
    'buckets, data',
    [
        (np.array([]), np.array([10, 20, 30])),
        (np.array([100]), np.array([])),
    ],
)
def test_bucket_creator_transform_edge_cases(buckets: np.ndarray, data: np.ndarray) -> None:
    bucket_creator = BucketCreator(buckets)

    if buckets.size == 0:
        with pytest.raises(ValueError, match='buckets array cannot be empty'):
            bucket_creator.transform(data)
    else:
        transformed = bucket_creator.transform(data)
        assert transformed.size == data.size


@pytest.mark.parametrize(
    'buckets, data, expected',
    [
        (np.array([10, 20, 30]), np.array([5, 15, 25]), np.array([30, 10, 20])),
    ],
)
def test_bucket_creator_transform_oob(buckets: np.ndarray, data: np.ndarray, expected: np.ndarray) -> None:
    with pytest.raises(ValueError, match='input data must be greater than the minimum bucket value'):
        bucket_creator = BucketCreator(buckets)
        bucket_creator.transform(data)
