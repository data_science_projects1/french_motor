import numpy as np
import pytest

from scripts.encoding.interval_discretizer import IntervalDiscretizer


def test_interval_discretizer_fit_valid_input() -> None:
    edges = [[0, 10, 20], [0, 5, 15]]
    discretizer = IntervalDiscretizer(edges=edges, right=False)
    x = np.array([[5, 3], [15, 7]])

    discretizer = discretizer.fit(x)

    assert discretizer.edges == edges


def test_interval_discretizer_transform_single_feature() -> None:
    edges = [[0, 10, 20]]
    discretizer = IntervalDiscretizer(edges=edges, right=False)
    x = np.array([[5], [15], [25]])

    result = discretizer.transform(x)

    expected_result = np.array([[1], [2], [3]])
    assert np.array_equal(result, expected_result)


def test_interval_discretizer_transform_multi_feature() -> None:
    edges = [[0, 10, 20], [0, 5, 15]]
    discretizer = IntervalDiscretizer(edges=edges, right=False)
    x = np.array([[5, 3], [15, 7], [25, 17]])

    result = discretizer.transform(x)

    expected_result = np.array([[1, 1], [2, 2], [3, 3]])
    assert np.array_equal(result, expected_result)


def test_interval_discretizer_right_bound_inclusion() -> None:
    edges = [[0, 10, 20]]
    discretizer = IntervalDiscretizer(edges=edges, right=True)
    x = np.array([[10], [20]])

    result = discretizer.transform(x)

    expected_result = np.array([[1], [2]])
    assert np.array_equal(result, expected_result)


def test_interval_discretizer_left_bound_exclusion() -> None:
    edges = [[0, 10, 20]]
    discretizer = IntervalDiscretizer(edges=edges, right=False)
    x = np.array([[10], [20]])

    result = discretizer.transform(x)

    expected_result = np.array([[2], [3]])
    assert np.array_equal(result, expected_result)


def test_interval_discretizer_raises_error_on_edge_mismatch() -> None:
    edges = [[0, 10, 20]]
    discretizer = IntervalDiscretizer(edges=edges, right=False)
    x = np.array([[5, 3], [15, 7]])

    with pytest.raises(AssertionError, match='Number of edges do not match number of features.'):
        discretizer.fit(x)
