import numpy as np

from scripts.encoding.multi_hot_encoder import MultiHotEncoder


def test_multi_hot_encoder_fit() -> None:
    categories = [['A', 'B'], [1, 2]]
    encoder = MultiHotEncoder(categories=categories)
    assert encoder.fit() is encoder


def test_multi_hot_encoder_transform_single_column() -> None:
    categories = [['A', 'B', 'C']]
    encoder = MultiHotEncoder(categories=categories)
    data = np.array([['A'], ['C'], ['D']])
    transformed = encoder.transform(data)
    expected = np.array([[1], [1], [0]])
    np.testing.assert_array_equal(transformed, expected)


def test_multi_hot_encoder_transform_multi_column() -> None:
    categories = [['A', 'B']]
    encoder = MultiHotEncoder(categories=categories)
    data = np.array([['A'], ['B'], ['C']])
    transformed = encoder.transform(data)
    expected = np.array([[1], [1], [0]])
    np.testing.assert_array_equal(transformed, expected)


def test_multi_hot_encoder_empty_categories() -> None:
    categories: list = []
    encoder = MultiHotEncoder(categories=categories)
    data = np.array([['A', 1]])
    transformed = encoder.transform(data)
    expected = np.array([[0, 0]])
    np.testing.assert_array_equal(transformed, expected)


def test_multi_hot_encoder_unseen_categories() -> None:
    categories = [['A', 'B'], [1, 2]]
    encoder = MultiHotEncoder(categories=categories)
    data = np.array([['C', 3], ['D', 4]])
    transformed = encoder.transform(data)
    expected = np.array([[0, 0], [0, 0]])
    np.testing.assert_array_equal(transformed, expected)
