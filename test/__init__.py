from typing import Any, Iterable, Union

import numpy as np
from numpy.testing import assert_array_almost_equal
from sklearn.base import TransformerMixin


def get_transformation(transformation: TransformerMixin, input: Any) -> np.ndarray:
    x = np.array(object=input, dtype=object, ndmin=2)
    transformed_values = transformation.fit_transform(X=x)
    assert transformed_values.ndim == 2

    return transformed_values


def assert_transformed_values(
    transformation: TransformerMixin,
    input: Any,
    expected: Union[float, Iterable[float]],
) -> None:
    x = get_transformation(transformation=transformation, input=input)
    y = np.array(expected, ndmin=2)

    assert_array_almost_equal(x=x, y=y, decimal=5)
