from pathlib import Path
from typing import Iterable, Union

import arff
from pandas import DataFrame

from scripts.util.logging_and_wrapper import log_df


class DataScienceFrame(DataFrame):
    """
    A subclass of DataFrame tailored for data science operations.

    DataScienceFrame extends the functionality of a standard DataFrame by
    providing additional data manipulation methods specifically useful for
    data science tasks. It is designed to streamline workflows and simplify
    operations such as casting column types and cleaning column values.
    """

    @classmethod
    def load_arff_data(cls, path: Path, cols: Iterable) -> 'DataScienceFrame':
        data = arff.load(path)
        return DataScienceFrame(data=data, columns=cols)

    @log_df
    def cast_cols_to_int(self, cols: Union[list, str]) -> 'DataScienceFrame':
        cols = [cols] if isinstance(cols, str) else cols
        if isinstance(cols, list):
            self[cols] = self[cols].astype(dtype='int64')
        else:
            raise ValueError('cols must be str or list type!')
        return self

    @log_df
    def remove_char_from_cols(self, cols: Union[Iterable, str], char: str) -> 'DataScienceFrame':
        cols = [cols] if isinstance(cols, str) else cols
        if hasattr(cols, '__iter__'):
            for col in cols:
                self[col] = self[col].str.replace(char, '')
        else:
            raise ValueError('Columns cols are not iterable or string type!')
        return self
