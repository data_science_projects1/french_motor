import logging
import time
from functools import wraps
from typing import Any, Callable

import pandas as pd

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def log_df(func: Callable) -> Callable:
    """
    A decorator function for logging information about Pandas DataFrame results of a function.
    It logs the current timestamp, the name of the decorated function, and the shape of the
    DataFrame if the function's output has a 'shape' attribute.

    Arguments:
        func (Callable): The function being decorated, expected to return a Pandas DataFrame.

    Returns:
        Callable: The wrapped function that logs details about its result before returning it.
    """

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> pd.DataFrame:
        df_result = func(*args, **kwargs)

        current_time: str = str(time.strftime('%Y%m%d%H%M%S'))

        msg: str = f'{current_time}: function: {func.__name__}'
        msg_shape: str = f', df shape: {df_result.shape}' if hasattr(df_result, 'shape') else ''
        logger.info(msg + msg_shape)

        return df_result

    return wrapper
