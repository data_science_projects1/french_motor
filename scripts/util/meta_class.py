class MetaClass:
    """
    A metaclass utility class for retrieving keys and values of annotated class
    variables.

    This class provides two class methods that extract the keys and values of
    statically defined annotated class attributes. It is primarily designed for
    use cases requiring introspection of class-level attributes in a standardized
    manner.
    """

    @classmethod
    def get_key_list(cls) -> list:
        cls_variables = cls.__dict__.get('__annotations__')
        if cls_variables is None:
            raise ValueError('No class variables declared!')

        return list(cls_variables.keys())

    @classmethod
    def get_value_list(cls) -> list:
        cls_var_keys: list = cls.get_key_list()
        return [cls.__dict__[cls_var_key] for cls_var_key in cls_var_keys]
