from typing import Any


class ClassProperty:
    """
    Descriptor that provides a class-level method-like property.

    This class is utilized to create a property that can be accessed at the class level
    and behaves like a method. It employs the descriptor protocol to call a method of
    the class as if it were a property, avoiding the need to manually define getter logic
    in the class.

    Attributes:
        fget (Any): The class method that the property wraps. This is set using the
        provided method reference in the initializer.

    """

    def __init__(self, fget: Any) -> None:
        self.fget: Any = classmethod(fget)

    def __get__(self, _: Any, owner: Any) -> Any:
        return self.fget.__get__(None, owner)()
