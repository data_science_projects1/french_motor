from pathlib import Path
from typing import Final

ROOT_FOLDER: Final[Path] = Path(__file__).parent.parent.parent.resolve()

PATH_TO_FREQ: Final[Path] = ROOT_FOLDER / Path('./data/claim_data/freMTPL2freq.arff')
PATH_TO_SEV: Final[Path] = ROOT_FOLDER / Path('./data/claim_data/freMTPL2sev.arff')

PATH_TO_REGIO_100k: Final[Path] = ROOT_FOLDER / Path('./data/france_shapefile/regions-20140306-100m.shp')
