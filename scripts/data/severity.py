from typing import Final

from scripts.data.file_paths import PATH_TO_SEV
from scripts.util import DataScienceFrame, log_df, logger


class Severity:
    """
    Represents the severity calculation and processing for claims data.

    Provides methods for loading data, grouping claims by policy ID, and processing
    large claims based on defined thresholds. This class is designed for handling
    insurance claim data where large claims are truncated and redistributed in a
     controlled manner. It incorporates an iterative process to redistribute claim
     amounts until a satisfactory balance is achieved.

    Attributes:
        id_pol: Represents the column name for policy ID used in the dataset.
        claim_amount: Represents the column name for claim amount in the dataset.
        claim_amount_capped: Represents the column name for capped claim amounts.
        KUPIERUNG_CLAIM_AMOUNT_LIMIT: The threshold defining the maximum allowed
            claim amount; limits larger claims for redistribution.
        KUPIERUNG_CLAIM_AMOUNT_QUANTIL_CHECK: A check threshold for determining
            quantile limits in the dataset.

    """

    id_pol: str = 'IDpol'
    claim_amount: str = 'ClaimAmount'
    claim_amount_capped: str = 'claim_amount_capped'

    KUPIERUNG_CLAIM_AMOUNT_LIMIT: Final[float] = 50_000  # only 0.35% are larger than limit
    KUPIERUNG_CLAIM_AMOUNT_QUANTIL_CHECK: Final[float] = 0.95

    @classmethod
    @log_df
    def main(cls) -> DataScienceFrame:
        df_severity = (
            cls._load_data()
            .cast_cols_to_int(cols=cls.id_pol)
            .pipe(cls._group_claims_by_policy_id)
            .pipe(cls._kupierung_large_claims_and_reallocate_truncated_claim_amount)
        )

        return df_severity

    @classmethod
    def _get_file_col_names(cls) -> list:
        return [cls.id_pol, cls.claim_amount]  # order of cols is important

    @classmethod
    @log_df
    def _load_data(cls) -> DataScienceFrame:
        return DataScienceFrame.load_arff_data(path=PATH_TO_SEV, cols=cls._get_file_col_names())

    @classmethod
    @log_df
    def _group_claims_by_policy_id(cls, df_severity: DataScienceFrame) -> DataScienceFrame:
        group_axis = {cls.claim_amount: (cls.claim_amount, 'sum')}
        df_severity = df_severity.groupby([cls.id_pol], as_index=False).agg(**group_axis)

        return df_severity

    @classmethod
    @log_df
    def _kupierung_large_claims_and_reallocate_truncated_claim_amount(
        cls,
        df_severity: DataScienceFrame,
    ) -> DataScienceFrame:
        df_severity[cls.claim_amount_capped] = 0.0

        while True:
            df_threshold = df_severity[cls.claim_amount] > cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT
            df_below_threshold = df_severity[cls.claim_amount] <= cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT

            df_kupierung = df_severity.loc[df_threshold]

            claim_amount_without_large_claims = df_severity.loc[df_below_threshold][cls.claim_amount].sum()
            claim_amount_total = df_severity[cls.claim_amount].sum()

            df_severity.loc[df_threshold, cls.claim_amount_capped] = (
                df_severity[cls.claim_amount] - cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT
            )

            claim_amount_capped = df_severity[df_threshold][cls.claim_amount_capped].sum()

            factor = (claim_amount_without_large_claims + claim_amount_capped) / (claim_amount_without_large_claims)

            logger.warning(
                f'claim sum total: {claim_amount_total} \n'
                f'Number of claims above kupierung threshold {cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT}: \n'
                f'{df_kupierung.shape[0]} out of {df_severity.shape[0]}. \n'
                f'Reset them to {cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT}. \n'
                f'Applied factor {factor}. \n',
            )

            df_severity.loc[df_threshold, cls.claim_amount] = cls.KUPIERUNG_CLAIM_AMOUNT_LIMIT

            df_severity.loc[df_below_threshold, cls.claim_amount] = df_severity[cls.claim_amount] * factor

            if factor < 1.0001:
                break

        return df_severity
