from pandera import Check, Column, DataFrameSchema

from scripts.data.severity import Severity
from scripts.util import DataScienceFrame


def main_schema_validation_severity(df_severity: DataScienceFrame) -> DataScienceFrame:
    """
    Validates the schema of a DataFrame for severity data.

    This function checks the structure and data types of a given DataFrame to ensure
    that it conforms to the defined schema for severity data. The schema includes
    specific rules for column types, nullability, and additional data validation steps.
    If the data does not conform to the schema, a validation error will be raised.
    If the validation is successful, the same DataFrame is returned.

    Args:
        df_severity: Input DataFrame containing severity data. Must comply with
                     the specified schema.

    Returns:
        The validated DataFrame with severity data, adhering to the required schema.
    """
    schema = DataFrameSchema(
        {
            Severity.id_pol: Column(dtype=int, required=True, nullable=False),
            Severity.claim_amount: Column(
                dtype=float,
                required=True,
                nullable=False,
                checks=Check.greater_than(min_value=0),
            ),
        },
    )

    schema.validate(df_severity)

    return df_severity
