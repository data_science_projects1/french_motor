from pandera import Check, Column, DataFrameSchema

from scripts.data.frequency import (
    Area,
    Frequency,
    FuelType,
    Region,
    VehicleBrand,
)
from scripts.util import DataScienceFrame


def main_schema_validation_frequency(df_frequency: DataScienceFrame) -> DataScienceFrame:
    """
    Validates the schema of a frequency-related DataFrame.

    This function ensures that the provided DataFrame adheres to a predefined schema
    with specific constraints for each column related to various attributes such
    as area, bonus-malus, claims, density, driver age, exposure, policy ID, region,
    vehicle age, brand, fuel type, and power. Each column of the input DataFrame is
    checked against its respective attributes' data type, requirements, and any
    applicable boundaries or permissible values.

    Parameters
    ----------
    df_frequency : DataScienceFrame
        The input DataFrame containing frequency-related data to be validated.

    Returns
    -------
    DataScienceFrame
        The validated input DataFrame if it conforms to the defined schema.

    Raises
    ------
    ValidationError
        If the input DataFrame does not conform to the defined schema.
    """
    schema = DataFrameSchema(
        {
            Frequency.area: Column(dtype=str, required=True, nullable=False, checks=Check.isin(Area.get_value_list())),
            Frequency.bonus_malus: Column(
                dtype=int,
                required=True,
                nullable=False,
                checks=Check.between(
                    min_value=Frequency.BONUS_MALUS_RANGE.min,
                    max_value=Frequency.BONUS_MALUS_RANGE.max,
                ),
            ),
            Frequency.claim_nb: Column(dtype=int, required=True, nullable=False, checks=Check.ge(min_value=0)),
            Frequency.density: Column(dtype=int, required=True, nullable=False, checks=Check.ge(min_value=1)),
            Frequency.driv_age: Column(
                dtype=int,
                required=True,
                nullable=False,
                checks=Check.between(
                    min_value=Frequency.DRIVER_AGE_RANGE.min,
                    max_value=Frequency.DRIVER_AGE_RANGE.max,
                ),
            ),
            Frequency.exposure: Column(dtype=float, required=True, nullable=False, checks=Check.gt(min_value=0)),
            Frequency.id_pol: Column(dtype=int, required=True, unique=True, nullable=False),
            Frequency.region: Column(
                dtype=str,
                required=True,
                nullable=False,
                checks=Check.isin(allowed_values=Region.get_value_list()),
            ),
            Frequency.veh_age: Column(
                dtype=int,
                required=True,
                nullable=False,
                checks=Check.between(
                    min_value=Frequency.VEHICLE_AGE_RANGE.min,
                    max_value=Frequency.VEHICLE_AGE_RANGE.max,
                ),
            ),
            Frequency.veh_brand: Column(
                dtype=str,
                required=True,
                nullable=False,
                checks=Check.isin(allowed_values=VehicleBrand.get_value_list()),
            ),
            Frequency.veh_gas: Column(
                dtype=str,
                required=True,
                nullable=False,
                checks=Check.isin(allowed_values=FuelType.get_value_list()),
            ),
            Frequency.veh_power: Column(dtype=int, required=True, nullable=False),
        },
    )

    schema.validate(df_frequency)

    return df_frequency
