from scripts.data.frequency import Frequency
from scripts.data.severity import Severity
from scripts.util import DataScienceFrame, log_df, logger


def main_plausibility_checks(
    df_modeling: DataScienceFrame,
    df_severity: DataScienceFrame,
    df_frequency: DataScienceFrame,
) -> None:
    _ = (
        df_modeling.pipe(_check_claim_zero_but_claim_amount_not)
        .pipe(_check_for_zero_claims)
        .pipe(_check_for_zero_exposure)
        .pipe(_check_kupierung_limit_claims)
        .pipe(_check_kupierung_limit_exposure)
        .pipe(_check_kupierung_limit_claim_number)
        .pipe(_check_for_missing_values)
    )

    _check_that_every_claim_is_joined(df_severity=df_severity, df_frequency=df_frequency)


@log_df
def _check_claim_zero_but_claim_amount_not(df_modeling: DataScienceFrame) -> DataScienceFrame:
    df_test = df_modeling.loc[(df_modeling[Frequency.claim_nb] == 0) & (df_modeling[Severity.claim_amount] > 0)]

    if df_test.shape[0] > 0:
        raise ValueError('Rows found with 0 claims but with claim amount > 0')

    return df_modeling


@log_df
def _check_for_zero_claims(df_modeling: DataScienceFrame) -> DataScienceFrame:
    df_test = df_modeling.loc[(df_modeling[Frequency.claim_nb] > 0) & (df_modeling[Severity.claim_amount] == 0)]

    if df_test.shape[0] > 0:
        raise ValueError('Rows found with >0 claims but with no claim amount.')

    return df_modeling


@log_df
def _check_for_zero_exposure(df_modeling: DataScienceFrame) -> DataScienceFrame:
    df_test = df_modeling.loc[(df_modeling[Frequency.exposure] <= 0)]

    if df_test.shape[0] > 0:
        raise ValueError('Rows found with 0 exposure.')

    return df_modeling


@log_df
def _check_kupierung_limit_claims(df_modeling: DataScienceFrame) -> DataScienceFrame:
    claim_amount_quantil: float = df_modeling.loc[df_modeling[Severity.claim_amount] > 0][
        Severity.claim_amount
    ].quantile(
        q=Severity.KUPIERUNG_CLAIM_AMOUNT_QUANTIL_CHECK,
    )

    if Severity.KUPIERUNG_CLAIM_AMOUNT_LIMIT < claim_amount_quantil:
        logger.warning(
            f'KUPIERUNG_LIMIT {Severity.KUPIERUNG_CLAIM_AMOUNT_LIMIT} lower than \n'
            f'KUPIERUNG_QUANTIL_CHECK {Severity.KUPIERUNG_CLAIM_AMOUNT_QUANTIL_CHECK}!',
        )

    return df_modeling


@log_df
def _check_kupierung_limit_exposure(df_modeling: DataScienceFrame) -> DataScienceFrame:
    df_test = df_modeling.loc[df_modeling[Frequency.exposure] > Frequency.KUPIERUNG_WEIGHT]

    if df_test.shape[0] > 0:
        logger.warning(
            f'Rows found with observation weight greater than KUPIERUNG_WEIGHT {Frequency.KUPIERUNG_WEIGHT}!',
        )

    return df_modeling


@log_df
def _check_kupierung_limit_claim_number(df_modeling: DataScienceFrame) -> DataScienceFrame:
    claim_number_quantil = df_modeling.loc[df_modeling[Frequency.claim_nb] > 0][Frequency.claim_nb].quantile(
        q=Frequency.KUPIERUNG_CLAIM_NB_QUANTIL_CHECK,
    )

    if Frequency.KUPIERUNG_CLAIM_NB < claim_number_quantil:
        logger.warning(
            f'KUPIERUNG_CLAIM_NB {Frequency.KUPIERUNG_CLAIM_NB} lower than \n'
            f'KUPIERUNG_CLAIM_NB_QUANTIL_CHECK {Frequency.KUPIERUNG_CLAIM_NB_QUANTIL_CHECK}!',
        )

    return df_modeling


@log_df
def _check_for_missing_values(df_modeling: DataScienceFrame) -> DataScienceFrame:
    if df_modeling.isnull().sum().sum() > 0:
        logger.info(df_modeling.isnull().sum())
        raise ValueError('Missing values found in at least one of the columns!')

    return df_modeling


def _check_that_every_claim_is_joined(
    df_severity: DataScienceFrame,
    df_frequency: DataScienceFrame,
) -> None:
    df_test = df_severity.loc[~df_severity[Severity.id_pol].isin(df_frequency[Frequency.id_pol].unique())]

    if df_test.shape[0] > 0:
        logger.warning(
            f'Not every claim could be mapped through policy id! '
            f'number of claims: {df_test.shape[0]}. '
            f'Claims will be removed in preprocessing!',
        )
