from collections import namedtuple
from typing import Final, Union

from scripts.data.file_paths import PATH_TO_FREQ
from scripts.util import DataScienceFrame, MetaClass, log_df, logger


class Region(MetaClass):
    r_11: str = 'R11'
    r_21: str = 'R21'
    r_22: str = 'R22'
    r_23: str = 'R23'
    r_24: str = 'R24'
    r_25: str = 'R25'
    r_26: str = 'R26'
    r_31: str = 'R31'
    r_41: str = 'R41'
    r_42: str = 'R42'
    r_43: str = 'R43'
    r_52: str = 'R52'
    r_53: str = 'R53'
    r_54: str = 'R54'
    r_72: str = 'R72'
    r_73: str = 'R73'
    r_74: str = 'R74'
    r_82: str = 'R82'
    r_83: str = 'R83'
    r_91: str = 'R91'
    r_93: str = 'R93'
    r_94: str = 'R94'


class Area(MetaClass):
    a: str = 'A'
    b: str = 'B'
    c: str = 'C'
    d: str = 'D'
    e: str = 'E'
    f: str = 'F'


class VehicleBrand(MetaClass):
    b_1: str = 'B1'
    b_10: str = 'B10'
    b_11: str = 'B11'
    b_12: str = 'B12'
    b_13: str = 'B13'
    b_14: str = 'B14'
    b_2: str = 'B2'
    b_3: str = 'B3'
    b_4: str = 'B4'
    b_5: str = 'B5'
    b_6: str = 'B6'


class FuelType(MetaClass):
    regular: str = 'Regular'
    diesel: str = 'Diesel'


class Frequency:
    """
    Frequency class for managing data processing of vehicle frequency-related datasets.

    This class provides functionalities to load, process, and clean datasets related
    to vehicle statistics. It ensures specific data transformations such as type casting,
    removal of characters, and applying thresholds to manage data consistency and quality.

    Attributes
    ----------
    id_pol : str
        Column name for policy ID.
    claim_nb : str
        Column name for the number of claims.
    exposure : str
        Column name for exposure data.
    area : str
        Column name for area information.
    veh_power : str
        Column name for vehicle power specification.
    veh_age : str
        Column name for vehicle age.
    driv_age : str
        Column name for driver age.
    bonus_malus : str
        Column name for the bonus-malus score.
    veh_brand : str
        Column name for vehicle brand.
    veh_gas : str
        Column name for vehicle fuel type.
    density : str
        Column name for population density.
    region : str
        Column name for geographical region.

    VEHICLE_AGE_LIMIT : Final[int]
        Maximum allowed age for a vehicle in data processing.
    KUPIERUNG_WEIGHT : Final[float]
        Threshold weight for vehicle exposure during processing.
    KUPIERUNG_CLAIM_NB : Final[int]
        Threshold for the number of claims during processing.
    KUPIERUNG_CLAIM_NB_QUANTIL_CHECK : Final[float]
        Quantile threshold for claim number checks.

    BONUS_MALUS_RANGE : Final[tuple]
        Accepted range of bonus-malus scores.
    DRIVER_AGE_RANGE : Final[tuple]
        Valid range for driver ages.
    VEHICLE_AGE_RANGE : Final[tuple]
        Accepted range for vehicle ages.

    Methods
    -------
    main(cls) -> DataScienceFrame
        Main method to load, clean, and process dataset, returning the final processed data.

    _get_file_col_names(cls) -> list
        Returns the column names required for loading the file in sequential order.

    _load_data(cls) -> DataScienceFrame
        Loads the dataset from a specific path and ensures column order consistency.

    _kupierung(df_frequency: DataScienceFrame, col: str, threshold: Union[float, int]) -> DataScienceFrame
        Applies a threshold to a specific column in the dataset, clipping values above the threshold.
    """

    id_pol: str = 'IDpol'
    claim_nb: str = 'ClaimNb'
    exposure: str = 'Exposure'
    area: str = 'Area'
    veh_power: str = 'VehPower'
    veh_age: str = 'VehAge'
    driv_age: str = 'DrivAge'
    bonus_malus: str = 'BonusMalus'
    veh_brand: str = 'VehBrand'
    veh_gas: str = 'VehGas'
    density: str = 'Density'
    region: str = 'Region'

    VEHICLE_AGE_LIMIT: Final[int] = 30
    KUPIERUNG_WEIGHT: Final[float] = 1.00
    KUPIERUNG_CLAIM_NB: Final[int] = 3
    KUPIERUNG_CLAIM_NB_QUANTIL_CHECK: Final[float] = 0.95

    Range = namedtuple('Range', ['min', 'max'])
    BONUS_MALUS_RANGE: Final[tuple] = Range(min=50, max=230)
    DRIVER_AGE_RANGE: Final[tuple] = Range(min=18, max=100)
    VEHICLE_AGE_RANGE: Final[tuple] = Range(min=0, max=100)

    @classmethod
    @log_df
    def main(cls) -> DataScienceFrame:
        """
        Main class method to process and transform a DataFrame for advanced data analysis
        in a data science context. Performs several stages of data cleaning and
        manipulation, including casting columns to integer type, removing trailing and
        leading characters from specific string columns, and applying threshold-based
        transformations on specified numerical columns.

        Parameters:
        cols_to_cast_to_int : list
            A list of column names to be cast to integer type.
        cols_with_trailing_leading_char : tuple
            A tuple of column names from which leading and trailing characters will
            be removed.

        Returns:
        DataScienceFrame
            A processed DataFrame ready for further data analysis or modeling.
        """
        cols_to_cast_to_int: list = [
            cls.bonus_malus,
            cls.claim_nb,
            cls.driv_age,
            cls.id_pol,
            cls.veh_age,
            cls.veh_power,
            cls.density,
        ]
        cols_with_trailing_leading_char: tuple = (cls.area, cls.region, cls.veh_brand)

        df_frequency = (
            cls._load_data()
            .cast_cols_to_int(cols=cols_to_cast_to_int)
            .remove_char_from_cols(cols=cols_with_trailing_leading_char, char="'")
            .pipe(cls._kupierung, col=cls.exposure, threshold=cls.KUPIERUNG_WEIGHT)
            .pipe(cls._kupierung, col=cls.claim_nb, threshold=cls.KUPIERUNG_CLAIM_NB)
            .pipe(cls._kupierung, col=cls.veh_age, threshold=cls.VEHICLE_AGE_LIMIT)
        )

        return df_frequency

    @classmethod
    def _get_file_col_names(cls) -> list:  # order of cols is important for loading file
        file_cols: list = [
            cls.id_pol,
            cls.claim_nb,
            cls.exposure,
            cls.area,
            cls.veh_power,
            cls.veh_age,
            cls.driv_age,
            cls.bonus_malus,
            cls.veh_brand,
            cls.veh_gas,
            cls.density,
            cls.region,
        ]
        return file_cols

    @classmethod
    @log_df
    def _load_data(cls) -> DataScienceFrame:
        return DataScienceFrame.load_arff_data(path=PATH_TO_FREQ, cols=cls._get_file_col_names())

    @staticmethod
    @log_df
    def _kupierung(df_frequency: DataScienceFrame, col: str, threshold: Union[float, int]) -> DataScienceFrame:
        """
        Perform filtering and capping operation on a DataFrame column based on a specified threshold.

        Filters rows in the input DataFrame where the specified column has a value greater or equal
        to the provided threshold and creates a copy of these rows. Additionally, limits values in
        the given column to the threshold value and logs the number of affected rows.

        Args:
            df_frequency (DataScienceFrame): The input DataFrame to process.
            col (str): The name of the column to apply filtering and capping on.
            threshold (Union[float, int]): The value used as the threshold for filtering
                and capping.

        Raises:
            No specific exceptions are handled or raised directly by this method.

        Returns:
            DataScienceFrame: The updated DataFrame where the specified column values are capped
            at the given threshold.
        """
        df_kupierung = df_frequency.loc[df_frequency[col] >= threshold].copy()
        logger.warning(
            f'Number of rows with {col} above threshold {threshold}: '
            f'{df_kupierung.shape[0]} out of {df_frequency.shape[0]}. '
            f'Reset to {threshold}.',
        )
        df_frequency[col] = df_frequency[col].clip(upper=threshold)

        return df_frequency
