from typing import Any

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class BucketCreator(TransformerMixin, BaseEstimator):
    """
    A transformer class for bucketizing numeric data using a predefined set of buckets.

    BucketCreator is a scikit-learn compatible transformer based on TransformerMixin
    and BaseEstimator. It allows transforming numeric data into specific buckets guided
    by the provided buckets array. This class is useful for discretizing continuous
    variables as part of preprocessing pipelines.

    Attributes:
    buckets (np.ndarray): An array representing the thresholds of the buckets used
    for discretizing the data.

    Methods
    -------
    fit(*_ : Any) -> BucketCreator
        Fits the transformer with the input data. This implementation simply
        returns the instance as no fitting is required for this transformer.

    transform(x: np.ndarray) -> np.ndarray
        Transforms the input data to its buckets encoded form.
    """

    def __init__(self, buckets: np.ndarray) -> None:
        self.buckets = buckets

    def fit(self, *_: Any) -> 'BucketCreator':
        return self

    def transform(self, x: np.ndarray) -> np.ndarray:
        self._validate(x=x)

        return np.array([self.buckets[i - 1] for i in np.digitize(x, self.buckets, right=False)])

    def _validate(self, x: np.ndarray) -> None:
        if self.buckets.size == 0:
            raise ValueError('buckets array cannot be empty')
        if (x.size > 0) and (x.min() < self.buckets[0]):
            raise ValueError('input data must be greater than the minimum bucket value')
