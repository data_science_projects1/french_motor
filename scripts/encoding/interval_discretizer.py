from typing import Any, Collection, List

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class IntervalDiscretizer(TransformerMixin, BaseEstimator):
    """
    Applies interval discretization to numeric data.

    This class is designed for transforming continuous numerical features into discrete intervals
    by using specified edge values. It supports multidimensional data, where each feature can have
    its own set of interval edges. It is compatible with scikit-learn's TransformerMixin and
    BaseEstimator, allowing for seamless integration in scikit-learn pipelines.

    Attributes
    ----------
    edges : List[Collection[float]]
        List of collections representing the interval edges for each feature. Each collection
        corresponds to one feature and defines the cut points for discretization.
    right : bool
        Indicator specifying whether intervals are closed on the right or not.

    Methods
    -------
    fit(*_ : Any) -> IntervalDiscretizer
        Fits the transformer with the input data. This implementation simply
        returns the instance as no fitting is required for this transformer.

    transform(x: np.ndarray) -> np.ndarray
        Transforms the input data to its interval-discretized encoded form. Encodes each
        feature column based on the provided edges.
    """

    def __init__(self, edges: List[Collection[float]], right: bool = False) -> None:
        self.edges = edges
        self.right = right

    def fit(self, x: np.ndarray, *_: Any) -> 'IntervalDiscretizer':
        n_features = x.shape[1]

        assert len(self.edges) == n_features, 'Number of edges do not match number of features.'
        return self

    def transform(self, x: np.ndarray) -> np.ndarray:
        transformed = np.array(
            [np.digitize(x=x[:, i], bins=np.array(self.edges[i]), right=self.right) for i in range(x.shape[1])],
        )

        return transformed.T
