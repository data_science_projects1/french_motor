from typing import Any, Collection, List

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class MultiHotEncoder(TransformerMixin, BaseEstimator):
    """
    A transformer for multi-hot encoding categorical features.

    This class is designed to encode categorical data into a multi-hot encoded
    representation. It transforms input categorical data into a binary matrix
    indicating the presence or absence of each category within specified feature
    columns. It is compatible with scikit-learn's TransformerMixin and
    BaseEstimator, enabling seamless integration into machine learning pipelines.

    Attributes
    ----------
    categories : List[Collection[Any]]
        List of collections where each collection defines the set of target
        categories for a corresponding feature column.

    Methods
    -------
    fit(*_ : Any) -> MultiHotEncoder
        Fits the transformer with the input data. This implementation simply
        returns the instance as no fitting is required for this transformer.

    transform(x: np.ndarray) -> np.ndarray
        Transforms the input data to its multi-hot encoded form. Encodes each
        feature column based on the provided categories.
    """

    def __init__(self, categories: List[Collection[Any]]) -> None:
        self.categories = categories

    def fit(self, *_: Any) -> 'MultiHotEncoder':
        return self

    def transform(self, x: np.ndarray) -> np.ndarray:
        return np.isin(x, np.array(self.categories)).astype(int)
