from typing import Any

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class IntervalBinarizer(TransformerMixin, BaseEstimator):
    """
    This class implements a binarizer that transforms numerical data into binary
    values based on specified lower and upper thresholds.

    The purpose of the class is to allow binary classification of values within a
    specified interval defined by a lower and an upper threshold. Data that falls
    within this interval is assigned a binary value of 1, while data outside this
    range is assigned a binary value of 0.

    Attributes:
        lower_threshold (float): The lower bound of the interval for binarization.
        upper_threshold (float): The upper bound of the interval for binarization.

    Methods
    -------
    fit(*_ : Any) -> IntervalBinarizer
        Fits the transformer with the input data. This implementation simply
        returns the instance as no fitting is required for this transformer.

    transform(x: np.ndarray) -> np.ndarray
        Transforms the input data to its interval-binarized encoded form. Encodes each
        feature column based on the provided interval edges.
    """

    def __init__(self, lower_threshold: float, upper_threshold: float) -> None:
        if lower_threshold >= upper_threshold:
            raise ValueError('upper_threshold should be greater than lower_threshold')

        self.lower_threshold = lower_threshold
        self.upper_threshold = upper_threshold

    def fit(self, *_: Any) -> 'IntervalBinarizer':
        return self

    def transform(self, x: np.ndarray) -> np.ndarray:
        return ((self.lower_threshold <= x) & (x < self.upper_threshold)).astype(int)
