from typing import Any

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class Rounder(TransformerMixin, BaseEstimator):
    """
    A transformer class for rounding numerical data into specified bucket sizes.

    The Rounder class inherits from TransformerMixin and BaseEstimator, making it
    compatible with scikit-learn's Transformer API. It is specifically designed
    to round input numerical data into predefined bucket sizes for preprocessing
    purposes.

    Attributes
    ----------
    buckets : int
        The size of the buckets into which the numerical data will be rounded.

    Methods
    -------
    fit(*_)
        This method is part of the scikit-learn Transformer API. It does not
        perform any fitting operations as this class does not require model
        training.

    transform(x)
        Rounds the input numerical data into specified bucket sizes and returns
        the transformed array.
    """

    def __init__(self, buckets: int) -> None:
        self.buckets = buckets

    def fit(self, *_: Any) -> 'Rounder':
        return self

    def transform(self, x: np.ndarray) -> np.ndarray:
        return (np.round(x.astype(float) / self.buckets, 0) * self.buckets).astype('int64')
