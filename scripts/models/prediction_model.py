from abc import abstractmethod
from functools import partial
from inspect import getfullargspec
from typing import Any

from sklearn.metrics import (
    mean_absolute_error,
    mean_tweedie_deviance,
    root_mean_squared_error,
)
from sklearn.model_selection import train_test_split as sklearn_train_test_split

from scripts.plotting import plot_data_agg
from scripts.util import DataScienceFrame, logger


class PredictionModel:
    def __init__(self, response: str, explanatory: list, weights: str, observed_target: str) -> None:
        self.model_scores = None
        self.response = response
        self.explanatory = explanatory
        self.weights = weights
        self.observed_target = observed_target

    def train_test_split(self, df_modeling: DataScienceFrame, test_size: float = 0.3, random_state: int = 42) -> None:
        self.df_full = df_modeling
        self.y = df_modeling[[self.response, self.weights, self.observed_target]]
        (
            self.x_train,
            self.x_test,
            self.y_train,
            self.y_test,
        ) = sklearn_train_test_split(
            df_modeling,
            self.y,
            random_state=random_state,
            test_size=test_size,
        )

    @abstractmethod
    def fit_model(self, model: Any) -> Any:
        self.model = model.fit()

    def calc_model_scores(self, tweedie_powers: list | None = None) -> None:
        """Evaluate an estimator on train and test set with different metrics"""

        metrics = [
            (
                'D² explained',
                None,
            ),  # Use default scorer if it exists (percentage of deviance explained)
            ('MAE', mean_absolute_error),
            ('RMSE', root_mean_squared_error),
        ]

        if tweedie_powers:
            metrics += [
                (
                    f'mean Tweedie dev p={power:.4f}',
                    partial(mean_tweedie_deviance, power=power),
                )
                for power in tweedie_powers
            ]

        res = []
        for subset_label, x, df in [
            ('train', self.x_train[self.explanatory], self.y_train),
            ('test', self.x_test[self.explanatory], self.y_test),
        ]:
            y, _weights = df[self.response], df[self.weights]
            for score_label, metric in metrics:
                if isinstance(self.model, tuple) and len(self.model) == 2:
                    est_freq, est_sev = self.model
                    y_pred = est_freq.predict(x) * est_sev.predict(x)
                else:
                    y_pred = self.model.predict(x)

                if metric is None:
                    if not hasattr(self.model, 'score'):
                        continue
                    if 'sample_weight' in getfullargspec(self.model.score).args:
                        score = self.model.score(x, y, sample_weight=_weights)
                    else:
                        score = self.model.score(x, y)
                else:
                    score = metric(y, y_pred, sample_weight=_weights)

                res.append(
                    {
                        'subset': subset_label,
                        'metric': score_label,
                        'score': f'{score:.4f}',
                    },
                )

        self.model_scores = (
            DataScienceFrame(res).set_index(['metric', 'subset']).score.unstack(-1).round(4).loc[:, ['train', 'test']]
        )

    def add_prediction(self) -> None:
        dataframes = [self.df_full, self.x_train, self.x_test]
        for df in dataframes:
            df['prediction'] = self.model.predict(df[self.explanatory])

        self.df_full['prediction_weighted'] = self.df_full.prediction * self.df_full[self.weights]
        self.x_train['prediction_weighted'] = self.x_train.prediction * self.y_train[self.weights]
        self.x_test['prediction_weighted'] = self.x_test.prediction * self.y_test[self.weights]

    def log_observed_vs_predicted(self) -> None:
        def _log_data(
            dataset_label: str,
            predicted_data: DataScienceFrame,
            observed_data: DataScienceFrame,
            weights: str,
        ) -> None:
            logger.info(
                f'predicted {dataset_label}: {predicted_data.prediction_weighted.sum() / observed_data[weights].sum()}',
            )
            logger.info(
                f'observed {dataset_label}: {observed_data[self.observed_target].sum() / observed_data[weights].sum()}',
            )
            logger.info(f'total predicted {dataset_label}: {predicted_data.prediction_weighted.sum()}')
            logger.info(f'total observed {dataset_label}: {observed_data[self.observed_target].sum()}')

        _log_data('full data', self.df_full, self.df_full, self.weights)
        _log_data('test data', self.x_test, self.y_test, self.weights)
        _log_data('train data', self.x_train, self.y_train, self.weights)

    def plot_results(self, df_to_plot: DataScienceFrame, cols: list) -> None:
        for col in cols:
            without_factor = {
                'df_input': df_to_plot,
                'columns': [col],
                'feature_y': self.weights,
                'feature_y_agg': 'sum',
                'feature_y_name': 'sum of weights',
                'feature_z1': (self.observed_target, self.weights),
                'feature_z1_name': 'observed',
                'feature_z1_agg': ('sum', 'sum'),
                'feature_z1_ops': '/',
                'feature_z2': ('prediction_weighted', self.weights),
                'feature_z2_name': 'predicted',
                'feature_z2_agg': ('sum', 'sum'),
                'feature_z2_ops': '/',
                'show_tables': False,
                'sort_cols': False,
                'chart_size_horizontal': 15,
                'chart_size_vertical': 8,
                'scale_z_axis_on_same_scale': ['observed', 'predicted'],
            }
            factors = {}
            if col + '_factor_link_fnc' in df_to_plot.columns.to_list():
                factors = {
                    'feature_z3': (col + '_factor_link_fnc', self.weights),
                    'feature_z3_name': 'factor',
                    'feature_z3_agg': ('sum', 'count'),
                    'feature_z3_ops': '/',
                    'chart_title': 'Observed vs. Predicted and Model Factors',
                }
            else:
                without_factor['chart_title'] = 'Observed vs. Predicted'

            kwargs = {**without_factor, **factors}
            plot_data_agg(**kwargs)
