from scripts.data import Modeling
from scripts.models.ensemble.ensemble_model import EnsembleModel, ensemble_model
from scripts.models.glm.frequency.freq_model import main_frequency_model
from scripts.models.glm.severity.severity_model import main_severity_model
from scripts.models.glm.tweedie.tweedie_model import main_tweedie_model

cols = [
    Modeling.area,
    Modeling.bonus_malus,
    Modeling.bonus_malus_grouped,
    Modeling.density,
    Modeling.density_grouped,
    Modeling.driv_age,
    Modeling.region,
    Modeling.veh_age,
    Modeling.veh_brand,
    Modeling.veh_gas,
    Modeling.veh_power,
    Modeling.veh_power_x_veh_gas,
    Modeling.veh_power_x_young_driver_flag,
    Modeling.young_driver_flag,
]

__all__ = [
    'ensemble_model',
    'main_frequency_model',
    'main_severity_model',
    'main_tweedie_model',
    'cols',
    'EnsembleModel',
]
