from typing import Any, Final, Tuple

import numpy as np
import statsmodels.api as sm
from IPython.display import display
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import FeatureUnion, Pipeline

from scripts.data import Area, FuelType, Modeling, Region, VehicleBrand
from scripts.encoding import (
    BucketCreator,
    IntervalBinarizer,
    IntervalDiscretizer,
    MultiHotEncoder,
)
from scripts.models.glm.glm_model import StatsModelGLM, main_glm_model
from scripts.models.glm.glm_preprocessor import ModelPreprocessor
from scripts.util import DataScienceFrame

BINS_BONUS_MALUS: Final[np.ndarray] = np.array([0, 50, 51, 54, 57, 60, 64, 68, 72, 76, 80, 85, 90, 95, 100])

EXPLANATORY_COLS: Final[list] = [
    Modeling.area,
    Modeling.bonus_malus,
    Modeling.driv_age,
    Modeling.region,
    Modeling.veh_age,
    Modeling.veh_brand,
    Modeling.veh_gas,
    Modeling.veh_power,
]


class FrequencyModelEncoders:
    area = FeatureUnion(
        [
            (Modeling.area + '_group_1', MultiHotEncoder([[Area.b]])),
            (Modeling.area + '_group_2', MultiHotEncoder([[Area.c]])),
            (Modeling.area + '_group_3', MultiHotEncoder([[Area.d]])),
            (Modeling.area + '_group_4', MultiHotEncoder([[Area.e, Area.f]])),
        ],
    )

    bonus_malus = Pipeline(
        steps=[
            ('grouping', BucketCreator(buckets=BINS_BONUS_MALUS)),
            (
                'groups',
                FeatureUnion(
                    [
                        (
                            Modeling.bonus_malus + '_group_1',
                            IntervalDiscretizer([[51, 54, 57, 60, 64, 68, 72, 76, 80, 85, 90, 95, 100]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_2',
                            IntervalBinarizer(lower_threshold=57, upper_threshold=60),
                        ),
                        (
                            Modeling.bonus_malus + '_group_3',
                            IntervalBinarizer(lower_threshold=60, upper_threshold=64),
                        ),
                        (
                            Modeling.bonus_malus + '_group_4',
                            IntervalDiscretizer([[100]]),
                        ),
                    ],
                ),
            ),
        ],
    )

    driver_age = FeatureUnion(
        [
            (
                Modeling.driv_age + '_group_1',
                IntervalBinarizer(lower_threshold=0, upper_threshold=25),
            ),
            (
                Modeling.driv_age + '_group_2',
                IntervalBinarizer(lower_threshold=0, upper_threshold=19),
            ),
            (
                Modeling.driv_age + '_group_3',
                IntervalBinarizer(lower_threshold=19, upper_threshold=20),
            ),
            (
                Modeling.driv_age + '_group_4',
                IntervalBinarizer(lower_threshold=20, upper_threshold=21),
            ),
            (
                Modeling.driv_age + '_group_5',
                IntervalBinarizer(lower_threshold=21, upper_threshold=22),
            ),
            (
                Modeling.driv_age + '_group_6',
                IntervalBinarizer(lower_threshold=22, upper_threshold=23),
            ),
            (
                Modeling.driv_age + '_group_7',
                IntervalBinarizer(lower_threshold=23, upper_threshold=24),
            ),
            (
                Modeling.driv_age + '_group_8',
                IntervalBinarizer(lower_threshold=24, upper_threshold=25),
            ),
            (
                Modeling.driv_age + '_group_9',
                IntervalBinarizer(lower_threshold=25, upper_threshold=33),
            ),
            (
                Modeling.driv_age + '_group_10',
                IntervalBinarizer(lower_threshold=33, upper_threshold=40),
            ),
        ],
    )

    region = FeatureUnion(
        [
            (
                Modeling.region + '_group_1',
                MultiHotEncoder([[Region.r_11, Region.r_41, Region.r_42, Region.r_43, Region.r_73]]),
            ),
            (Modeling.region + '_group_2', MultiHotEncoder([[Region.r_82, Region.r_74]])),
        ],
    )

    vehicle_age = FeatureUnion([(Modeling.veh_age + '_group_1', IntervalDiscretizer([[13, 15, 18]]))])

    vehicle_brand = FeatureUnion(
        [
            (Modeling.veh_brand + '_group_1', MultiHotEncoder([[VehicleBrand.b_11]])),
            (Modeling.veh_brand + '_group_2', MultiHotEncoder([[VehicleBrand.b_12, VehicleBrand.b_14]])),
            (
                Modeling.veh_brand + '_group_3',
                MultiHotEncoder(
                    [[VehicleBrand.b_3, VehicleBrand.b_4, VehicleBrand.b_5, VehicleBrand.b_6, VehicleBrand.b_13]],
                ),
            ),
        ],
    )

    vehicle_gas = FeatureUnion([(Modeling.veh_gas + '_group_1', MultiHotEncoder([[FuelType.regular]]))])

    vehicle_power = FeatureUnion(
        [
            (
                Modeling.veh_power + '_group_1',
                IntervalBinarizer(lower_threshold=0, upper_threshold=5),
            ),
            (Modeling.veh_power + '_group_2', IntervalDiscretizer([[8, 9, 10, 11]])),
        ],
    )

    @classmethod
    def get_preprocessing(cls) -> ColumnTransformer:
        preprocessing = ColumnTransformer(
            transformers=[
                (
                    Modeling.area,
                    cls.area,
                    [EXPLANATORY_COLS.index(Modeling.area)],
                ),
                (
                    Modeling.bonus_malus,
                    cls.bonus_malus,
                    [EXPLANATORY_COLS.index(Modeling.bonus_malus)],
                ),
                (
                    Modeling.driv_age,
                    cls.driver_age,
                    [EXPLANATORY_COLS.index(Modeling.driv_age)],
                ),
                (
                    Modeling.region,
                    cls.region,
                    [EXPLANATORY_COLS.index(Modeling.region)],
                ),
                (
                    Modeling.veh_age,
                    cls.vehicle_age,
                    [EXPLANATORY_COLS.index(Modeling.veh_age)],
                ),
                (
                    Modeling.veh_brand,
                    cls.vehicle_brand,
                    [EXPLANATORY_COLS.index(Modeling.veh_brand)],
                ),
                (
                    Modeling.veh_gas,
                    cls.vehicle_gas,
                    [EXPLANATORY_COLS.index(Modeling.veh_gas)],
                ),
                (
                    Modeling.veh_power,
                    cls.vehicle_power,
                    [EXPLANATORY_COLS.index(Modeling.veh_power)],
                ),
            ],
        )
        display(preprocessing)
        return preprocessing


class FrequencyModelPreprocessor(ModelPreprocessor):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.create_encoded_cols()

    def create_encoded_cols(self) -> None:
        self.preprocessing = FrequencyModelEncoders.get_preprocessing()

    def preprocess_data(self) -> Tuple[list, DataScienceFrame]:
        return super().preprocess_data()


def main_frequency_model(df_modeling: DataScienceFrame, cols_to_plot: list | None = None) -> StatsModelGLM:
    return main_glm_model(
        df_modeling=df_modeling,
        model_preprocessor=FrequencyModelPreprocessor,
        explanatory_cols=EXPLANATORY_COLS,
        response=Modeling.frequency,
        observed_target=Modeling.claim_nb,
        weights=Modeling.exposure,
        statsmodel=sm.families.Poisson(sm.families.links.Log()),
        cols_to_plot=cols_to_plot,
    )
