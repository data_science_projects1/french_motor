from typing import Any, Final, Tuple

import numpy as np
import statsmodels.api as sm
from IPython.display import display
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import FeatureUnion, Pipeline

from scripts.data import Area, FuelType, Modeling, Region, VehicleBrand
from scripts.encoding import (
    BucketCreator,
    IntervalBinarizer,
    IntervalDiscretizer,
    MultiHotEncoder,
    Rounder,
)
from scripts.models.glm.glm_model import StatsModelGLM, main_glm_model
from scripts.models.glm.glm_preprocessor import ModelPreprocessor
from scripts.util import DataScienceFrame

BINS_BONUS_MALUS: Final[np.ndarray] = np.array([0, 50, 51, 54, 57, 60, 64, 68, 72, 76, 80, 85, 90, 95, 100])
BINS_DENSITY: Final[np.ndarray] = np.array(
    [
        0,
        100,
        200,
        300,
        400,
        500,
        600,
        700,
        800,
        900,
        1000,
        1300,
        1400,
        1700,
        2100,
        2700,
        3300,
        3400,
        3700,
        4100,
        4300,
        4800,
        5400,
        8500,
        9300,
        10_000,
        27_000,
    ],
)

EXPLANATORY_COLS: Final[list] = [
    Modeling.area,
    Modeling.bonus_malus,
    Modeling.density,
    Modeling.driv_age,
    Modeling.region,
    Modeling.veh_age,
    Modeling.veh_brand,
    Modeling.veh_gas,
    Modeling.veh_power,
]


class TweedieEncoders:
    area = FeatureUnion(
        [
            (Modeling.area + '_group_1', MultiHotEncoder([[Area.a, Area.c, Area.e]])),
            (Modeling.area + '_group_2', MultiHotEncoder([[Area.d]])),
            (Modeling.area + '_group_3', MultiHotEncoder([[Area.f]])),
        ],
    )

    bonus_malus = Pipeline(
        steps=[
            ('grouping', BucketCreator(buckets=BINS_BONUS_MALUS)),
            (
                'groups',
                FeatureUnion(
                    [
                        (
                            Modeling.bonus_malus + '_group_1',
                            IntervalDiscretizer([[51]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_2',
                            IntervalDiscretizer([[54]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_3',
                            IntervalDiscretizer([[57]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_4',
                            IntervalDiscretizer([[72]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_5',
                            IntervalDiscretizer([[90, 95]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_6',
                            IntervalDiscretizer([[100]]),
                        ),
                        (
                            Modeling.bonus_malus + '_group_7',
                            IntervalBinarizer(lower_threshold=60, upper_threshold=61),
                        ),
                        (
                            Modeling.bonus_malus + '_group_8',
                            IntervalBinarizer(lower_threshold=76, upper_threshold=77),
                        ),
                    ],
                ),
            ),
        ],
    )

    density = Pipeline(
        steps=[
            ('rounding', Rounder(buckets=100)),
            ('grouping', BucketCreator(buckets=BINS_DENSITY)),
            (
                'groups',
                FeatureUnion(
                    [
                        (Modeling.density + '_group_1', IntervalDiscretizer([[100]])),
                        (
                            Modeling.density + '_group_2',
                            IntervalDiscretizer([[200, 400, 500, 600, 700, 800]]),
                        ),
                        (
                            Modeling.density + '_group_3',
                            IntervalBinarizer(lower_threshold=800, upper_threshold=1_000),
                        ),
                        (
                            Modeling.density + '_group_4',
                            IntervalBinarizer(lower_threshold=1_000, upper_threshold=1_400),
                        ),
                        (
                            Modeling.density + '_group_5',
                            MultiHotEncoder([[3_400, 5_400]]),
                        ),
                    ],
                ),
            ),
        ],
    )

    driver_age = FeatureUnion(
        [
            (
                Modeling.driv_age + '_group_1',
                IntervalBinarizer(lower_threshold=0, upper_threshold=25),
            ),
            (
                Modeling.driv_age + '_group_2',
                IntervalBinarizer(lower_threshold=0, upper_threshold=19),
            ),
            (
                Modeling.driv_age + '_group_3',
                IntervalBinarizer(lower_threshold=19, upper_threshold=20),
            ),
            (
                Modeling.driv_age + '_group_4',
                IntervalBinarizer(lower_threshold=20, upper_threshold=21),
            ),
            (
                Modeling.driv_age + '_group_5',
                IntervalBinarizer(lower_threshold=21, upper_threshold=22),
            ),
            (
                Modeling.driv_age + '_group_6',
                IntervalBinarizer(lower_threshold=22, upper_threshold=23),
            ),
            (
                Modeling.driv_age + '_group_7',
                IntervalBinarizer(lower_threshold=23, upper_threshold=28),
            ),
            (
                Modeling.driv_age + '_group_8',
                IntervalBinarizer(lower_threshold=28, upper_threshold=33),
            ),
            (
                Modeling.driv_age + '_group_9',
                IntervalBinarizer(lower_threshold=33, upper_threshold=40),
            ),
            (
                Modeling.driv_age + '_group_10',
                IntervalBinarizer(lower_threshold=72, upper_threshold=79),
            ),
        ],
    )

    region = FeatureUnion(
        [
            (Modeling.region + '_group_1', MultiHotEncoder([[Region.r_11, Region.r_23]])),
            (Modeling.region + '_group_2', MultiHotEncoder([[Region.r_93, Region.r_94]])),
            (Modeling.region + '_group_3', MultiHotEncoder([[Region.r_82]])),
            (Modeling.region + '_group_4', MultiHotEncoder([[Region.r_22]])),
            (Modeling.region + '_group_5', MultiHotEncoder([[Region.r_42, Region.r_73, Region.r_83]])),
            (Modeling.region + '_group_6', MultiHotEncoder([[Region.r_24, Region.r_72]])),
            (Modeling.region + '_group_7', MultiHotEncoder([[Region.r_91]])),
            (Modeling.region + '_group_8', MultiHotEncoder([[Region.r_74]])),
            (Modeling.region + '_group_9', MultiHotEncoder([[Region.r_25, Region.r_26]])),
        ],
    )

    vehicle_age = FeatureUnion(
        [
            (
                Modeling.veh_age + '_group_1',
                IntervalDiscretizer([[8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]]),
            ),
        ],
    )

    vehicle_brand = FeatureUnion(
        [
            (
                Modeling.veh_brand + '_group_1',
                MultiHotEncoder([[VehicleBrand.b_1, VehicleBrand.b_2, VehicleBrand.b_4]]),
            ),
            (Modeling.veh_brand + '_group_2', MultiHotEncoder([[VehicleBrand.b_3]])),
            (Modeling.veh_brand + '_group_3', MultiHotEncoder([[VehicleBrand.b_5]])),
            (Modeling.veh_brand + '_group_4', MultiHotEncoder([[VehicleBrand.b_6, VehicleBrand.b_12]])),
            (Modeling.veh_brand + '_group_5', MultiHotEncoder([[VehicleBrand.b_10]])),
            (Modeling.veh_brand + '_group_6', MultiHotEncoder([[VehicleBrand.b_11]])),
            (Modeling.veh_brand + '_group_7', MultiHotEncoder([[VehicleBrand.b_13]])),
        ],
    )

    vehicle_gas = FeatureUnion([(Modeling.veh_gas + '_group_1', MultiHotEncoder([[FuelType.regular]]))])

    vehicle_power = FeatureUnion(
        [
            (
                Modeling.veh_power + '_group_1',
                IntervalBinarizer(lower_threshold=4, upper_threshold=6),
            ),
            (
                Modeling.veh_power + '_group_2',
                IntervalDiscretizer([[9, 10, 11, 12, 13]]),
            ),
            (
                Modeling.veh_power + '_group_3',
                IntervalBinarizer(lower_threshold=9, upper_threshold=11),
            ),
            (
                Modeling.veh_power + '_group_4',
                IntervalBinarizer(lower_threshold=11, upper_threshold=12),
            ),
            (
                Modeling.veh_power + '_group_5',
                IntervalBinarizer(lower_threshold=12, upper_threshold=14),
            ),
        ],
    )

    @classmethod
    def get_preprocessing(cls) -> ColumnTransformer:
        preprocessing = ColumnTransformer(
            transformers=[
                (
                    Modeling.area,
                    cls.area,
                    [EXPLANATORY_COLS.index(Modeling.area)],
                ),
                (
                    Modeling.bonus_malus,
                    cls.bonus_malus,
                    [EXPLANATORY_COLS.index(Modeling.bonus_malus)],
                ),
                (
                    Modeling.density,
                    cls.density,
                    [EXPLANATORY_COLS.index(Modeling.density)],
                ),
                (
                    Modeling.driv_age,
                    cls.driver_age,
                    [EXPLANATORY_COLS.index(Modeling.driv_age)],
                ),
                (
                    Modeling.region,
                    cls.region,
                    [EXPLANATORY_COLS.index(Modeling.region)],
                ),
                (
                    Modeling.veh_age,
                    cls.vehicle_age,
                    [EXPLANATORY_COLS.index(Modeling.veh_age)],
                ),
                (
                    Modeling.veh_brand,
                    cls.vehicle_brand,
                    [EXPLANATORY_COLS.index(Modeling.veh_brand)],
                ),
                (
                    Modeling.veh_gas,
                    cls.vehicle_gas,
                    [EXPLANATORY_COLS.index(Modeling.veh_gas)],
                ),
                (
                    Modeling.veh_power,
                    cls.vehicle_power,
                    [EXPLANATORY_COLS.index(Modeling.veh_power)],
                ),
            ],
        )
        display(preprocessing)

        return preprocessing


class TweedieModelPreprocessor(ModelPreprocessor):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.create_encoded_cols()

    def create_encoded_cols(self) -> None:
        self.preprocessing = TweedieEncoders.get_preprocessing()

    def preprocess_data(self) -> Tuple[list, DataScienceFrame]:
        return super().preprocess_data()


def main_tweedie_model(df_modeling: DataScienceFrame, cols_to_plot: list | None = None) -> StatsModelGLM:
    return main_glm_model(
        df_modeling=df_modeling,
        model_preprocessor=TweedieModelPreprocessor,
        explanatory_cols=EXPLANATORY_COLS,
        response=Modeling.burning_cost,
        observed_target=Modeling.claim_amount,
        weights=Modeling.exposure,
        statsmodel=sm.families.Tweedie(var_power=1.25, link=sm.families.links.Log()),
        cols_to_plot=cols_to_plot,
    )
