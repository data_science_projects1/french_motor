from typing import Callable, Type

import mlflow
import numpy as np
import pandas as pd
import statsmodels.api as sm
from IPython.display import display

from scripts.models.glm.glm_preprocessor import ModelPreprocessor
from scripts.models.prediction_model import PredictionModel
from scripts.util import DataScienceFrame


class StatsModelGLM(PredictionModel):
    def fit_model(
        self,
        statsmodel: sm.families.Gamma | sm.families.Tweedie | sm.families.Poisson,
        show_summary: bool = True,
    ) -> None:
        self.model_settings = sm.GLM(
            endog=self.y_train[self.response],
            exog=self.x_train[self.explanatory],
            freq_weights=self.y_train[self.weights],
            family=statsmodel,
        )

        self.model = self.model_settings.fit(maxiter=100)

        if show_summary:
            print(self.model.summary())
            display(DataScienceFrame(np.exp(self.model.params), columns=['factor']))

    def calc_cumulated_factors(self, cols: list, link_function: Callable = np.exp) -> None:
        self.df_full_plot = None
        self.x_test_plot = None
        self.x_train_plot = None

        df_to_add = (self.df_full, self.x_test, self.x_train)
        df_cls_plot = ['df_full_plot', 'x_test_plot', 'x_train_plot']

        for index, df in enumerate(df_to_add):
            model_coeff = self.model.params.to_numpy()

            df_linear_predictor = df[self.explanatory] * model_coeff
            df_linear_predictor = df_linear_predictor.add_suffix('_linear_predictor')

            df_plot = pd.merge(
                left=df,
                right=df_linear_predictor,
                left_index=True,
                right_index=True,
                how='left',
            )

            lp = df_linear_predictor.columns.to_list()

            result = []

            for col in cols:
                encoders = np.where(np.char.find(lp, col, start=0, end=None) > -1)
                cols_encoded = [lp[ix] for ix in encoders[0]]
                result.append([col, cols_encoded])

            for col in result:
                df_plot[col[0] + '_factor'] = 0.0

                for pred in col[1]:
                    df_plot[col[0] + '_factor'] = df_plot[col[0] + '_factor'] + df_plot[pred]

                df_plot[col[0] + '_factor_link_fnc'] = link_function(df_plot[col[0] + '_factor'])

            setattr(self, df_cls_plot[index], df_plot)


def main_glm_model(
    df_modeling: DataScienceFrame,
    model_preprocessor: Type[ModelPreprocessor],
    explanatory_cols: list,
    response: str,
    observed_target: str,
    weights: str,
    statsmodel: sm.families.Gamma | sm.families.Tweedie | sm.families.Poisson,
    cols_to_plot: list | None = None,
) -> StatsModelGLM:
    mlflow.set_tracking_uri('http://mlflow-server-french-motor:5000')

    with mlflow.start_run():
        # Log hyperparameters
        mlflow.log_param('response', response)
        mlflow.log_param('observed_target', observed_target)
        mlflow.log_param('weights', weights)
        mlflow.log_param('model_family', statsmodel.__class__.__name__)
        mlflow.log_param('explanatory_columns', explanatory_cols)

        encoded_cols, df_finetune = model_preprocessor(
            df_modeling=df_modeling,
            explanatory_cols=explanatory_cols,
        ).preprocess_data()

        glm_model = StatsModelGLM(
            response=response,
            explanatory=encoded_cols,
            weights=weights,
            observed_target=observed_target,
        )
        glm_model.train_test_split(df_modeling=df_finetune)

        glm_model.explanatory = encoded_cols

        if isinstance(statsmodel, sm.families.Tweedie):
            glm_model.fit_model(statsmodel=statsmodel, show_summary=False)
            optimal_var_power = glm_model.model_settings.estimate_tweedie_power(mu=glm_model.model.mu)
            glm_model.fit_model(
                statsmodel=sm.families.Tweedie(var_power=optimal_var_power, link=sm.families.links.Log()),
            )
        else:
            glm_model.fit_model(statsmodel=statsmodel)

        # Log model factors (coefficients)
        coefficients = np.exp(glm_model.model.params).to_dict()
        mlflow.log_metrics(coefficients)

        glm_model.calc_model_scores()
        print(glm_model.model_scores)

        # Log model scores
        for metric_name, metric_value in glm_model.model_scores.items():
            if isinstance(metric_value, pd.Series):
                metric_dict = metric_value.to_dict()  # Convert Series to dictionary
                mlflow.log_params({f'{metric_name}_{k}': v for k, v in metric_dict.items()})
            else:
                mlflow.log_metric(metric_name, metric_value)

        glm_model.add_prediction()
        glm_model.log_observed_vs_predicted()

        calc_cols = cols_to_plot if cols_to_plot else explanatory_cols
        glm_model.calc_cumulated_factors(cols=calc_cols, link_function=np.exp)

        # Log the model using MLflow's StatsModels flavor
        mlflow.statsmodels.log_model(glm_model.model, 'model')

    return glm_model
