from abc import abstractmethod
from typing import Tuple

from sklearn.compose import ColumnTransformer

from scripts.util import DataScienceFrame


class ModelPreprocessor:
    def __init__(self, df_modeling: DataScienceFrame, explanatory_cols: list) -> None:
        self.df_before_encoding = df_modeling
        self.explanatory_cols = explanatory_cols
        self.number_cols_before_encoding = df_modeling.shape[1]

    @abstractmethod
    def create_encoded_cols(self) -> None:
        self.preprocessing = ColumnTransformer()

    def preprocess_data(self) -> Tuple[list, DataScienceFrame]:
        df_finetune = self.df_before_encoding.copy()
        df_finetune['const'] = 1.0

        x = df_finetune[self.explanatory_cols].copy()

        preprocessing_result = self.preprocessing.fit_transform(x.to_numpy(dtype=object, na_value=None))

        encoder: int = 0
        for feature in list(self.preprocessing.named_transformers_.keys()):
            if hasattr(self.preprocessing.named_transformers_[feature], 'named_steps'):
                step = self.preprocessing.named_transformers_[feature]['groups']
            else:
                step = self.preprocessing.named_transformers_[feature]
            for group in list(step.named_transformers.keys()):
                if '_group' in group:
                    df_finetune[group] = preprocessing_result.T[encoder]
                encoder += 1

        encoded_cols: list = df_finetune.columns.to_list()[self.number_cols_before_encoding:]  # fmt: skip

        return encoded_cols, df_finetune
