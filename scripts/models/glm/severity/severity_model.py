from typing import Any, Final, Tuple

import numpy as np
import statsmodels.api as sm
from IPython.display import display
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import FeatureUnion, Pipeline

from scripts.data import Area, FuelType, Modeling
from scripts.encoding import (
    BucketCreator,
    IntervalBinarizer,
    IntervalDiscretizer,
    MultiHotEncoder,
)
from scripts.models.glm.glm_model import StatsModelGLM, main_glm_model
from scripts.models.glm.glm_preprocessor import ModelPreprocessor
from scripts.util import DataScienceFrame

BINS_BONUS_MALUS: Final[np.ndarray] = np.array([0, 50, 51, 54, 57, 60, 64, 68, 72, 76, 80, 85, 90, 95, 100])

EXPLANATORY_COLS: Final[list] = [
    Modeling.area,
    Modeling.bonus_malus,
    Modeling.driv_age,
    Modeling.veh_age,
    Modeling.veh_gas,
    Modeling.veh_power,
]


class SeverityModelEncoders:
    area = FeatureUnion(
        [
            (Modeling.area + '_group_1', MultiHotEncoder([[Area.b]])),
            (Modeling.area + '_group_2', MultiHotEncoder([[Area.c]])),
            (Modeling.area + '_group_3', MultiHotEncoder([[Area.d]])),
            (Modeling.area + '_group_4', MultiHotEncoder([[Area.e]])),
            (Modeling.area + '_group_5', MultiHotEncoder([[Area.f]])),
        ],
    )

    bonus_malus = Pipeline(
        steps=[
            ('grouping', BucketCreator(buckets=BINS_BONUS_MALUS)),
            (
                'groups',
                FeatureUnion([(Modeling.bonus_malus + '_group_1', IntervalDiscretizer([[100]]))]),
            ),
        ],
    )

    driver_age = FeatureUnion(
        [
            (
                Modeling.driv_age + '_group_1',
                IntervalBinarizer(lower_threshold=0, upper_threshold=19),
            ),
            (
                Modeling.driv_age + '_group_2',
                IntervalBinarizer(lower_threshold=19, upper_threshold=20),
            ),
            (
                Modeling.driv_age + '_group_3',
                IntervalBinarizer(lower_threshold=0, upper_threshold=25),
            ),
        ],
    )

    vehicle_age = FeatureUnion(
        [
            (
                Modeling.veh_age + '_group_1',
                IntervalBinarizer(lower_threshold=0, upper_threshold=1),
            ),
            (
                Modeling.veh_age + '_group_2',
                IntervalBinarizer(lower_threshold=9, upper_threshold=21),
            ),
            (
                Modeling.veh_age + '_group_3',
                IntervalBinarizer(lower_threshold=21, upper_threshold=999),
            ),
        ],
    )

    vehicle_gas = FeatureUnion([(Modeling.veh_gas + '_group_1', MultiHotEncoder([[FuelType.regular]]))])

    vehicle_power = FeatureUnion([(Modeling.veh_power + '_group_1', IntervalDiscretizer([[12]]))])

    @classmethod
    def get_preprocessing(cls) -> ColumnTransformer:
        preprocessing = ColumnTransformer(
            transformers=[
                (
                    Modeling.area,
                    cls.area,
                    [EXPLANATORY_COLS.index(Modeling.area)],
                ),
                (
                    Modeling.bonus_malus,
                    cls.bonus_malus,
                    [EXPLANATORY_COLS.index(Modeling.bonus_malus)],
                ),
                (
                    Modeling.driv_age,
                    cls.driver_age,
                    [EXPLANATORY_COLS.index(Modeling.driv_age)],
                ),
                (
                    Modeling.veh_age,
                    cls.vehicle_age,
                    [EXPLANATORY_COLS.index(Modeling.veh_age)],
                ),
                (
                    Modeling.veh_gas,
                    cls.vehicle_gas,
                    [EXPLANATORY_COLS.index(Modeling.veh_gas)],
                ),
                (
                    Modeling.veh_power,
                    cls.vehicle_power,
                    [EXPLANATORY_COLS.index(Modeling.veh_power)],
                ),
            ],
        )
        display(preprocessing)
        return preprocessing


class SeverityModelPreprocessor(ModelPreprocessor):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.create_encoded_cols()

    def create_encoded_cols(self) -> None:
        self.preprocessing = SeverityModelEncoders.get_preprocessing()

    def preprocess_data(self) -> Tuple[list, DataScienceFrame]:
        return super().preprocess_data()


def main_severity_model(df_modeling: DataScienceFrame, cols_to_plot: list | None = None) -> StatsModelGLM:
    return main_glm_model(
        df_modeling=df_modeling,
        model_preprocessor=SeverityModelPreprocessor,
        explanatory_cols=EXPLANATORY_COLS,
        response=Modeling.avg_claim_amount,
        observed_target=Modeling.claim_amount,
        weights=Modeling.claim_nb,
        statsmodel=sm.families.Gamma(sm.families.links.Log()),
        cols_to_plot=cols_to_plot,
    )
