from typing import Any, Final

from scripts.data import Modeling
from scripts.models.prediction_model import PredictionModel

EXPLANATORY_COLS: Final[list] = (
    [
        Modeling.bonus_malus_grouped,
        Modeling.density_grouped,
        Modeling.driv_age,
        Modeling.veh_age,
        Modeling.veh_power,
        Modeling.young_driver_flag,
        Modeling.veh_gas_encoded,
        Modeling.veh_power_x_young_driver_flag,
    ]
    + Modeling.AREA_ENCODED
    + Modeling.REGION_ENCODED
    + Modeling.VEH_BRAND_ENCODED
    + Modeling.IA_VEH_POWER_X_FUEL_TYPE
)


class EnsembleModel(PredictionModel):
    def fit_model(self, model: Any) -> None:
        self.model = model
        self.model.fit(
            self.x_train[self.explanatory],
            self.y_train[self.response],
            sample_weight=self.y_train[self.weights],
        )


ensemble_model = EnsembleModel(
    response=Modeling.burning_cost,
    explanatory=EXPLANATORY_COLS,
    weights=Modeling.exposure,
    observed_target=Modeling.claim_amount,
)
