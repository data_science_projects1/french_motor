import operator
from typing import Any, Final, Tuple, Union, cast

import matplotlib.pyplot as plt
import numpy as np
from IPython import get_ipython
from IPython.display import display
from pandas.api.types import is_numeric_dtype

from scripts.data import Modeling
from scripts.util import DataScienceFrame, logger

OPERATOR: Final[dict] = {'/': operator.truediv, '-': operator.sub}

ipy = get_ipython()


def plot_data_agg(
    df_input: DataScienceFrame,
    columns: list,
    feature_y: str,
    feature_y_agg: str,
    feature_y_name: str,
    feature_z1: Tuple[str, str] | None = None,
    feature_z1_name: str | None = None,
    feature_z1_agg: Tuple[str, str] | None = None,
    feature_z1_ops: str | None = None,
    feature_z1_scale: Tuple[float, float] | None = None,
    feature_z1_color: str = 'magenta',
    feature_z2: Tuple[str, str] | None = None,
    feature_z2_name: str | None = None,
    feature_z2_agg: Tuple[str, str] | None = None,
    feature_z2_ops: str | None = None,
    feature_z2_scale: Tuple[float, float] | None = None,
    feature_z2_color: str = 'green',
    feature_z3: Tuple[str, str] | None = None,
    feature_z3_name: str | None = None,
    feature_z3_agg: Tuple[str, str] | None = None,
    feature_z3_ops: str | None = None,
    feature_z3_scale: Tuple[float, float] | None = None,
    feature_z3_color: str = 'lime',
    feature_z4: Tuple[str, str] | None = None,
    feature_z4_name: str | None = None,
    feature_z4_agg: Tuple[str, str] | None = None,
    feature_z4_ops: str | None = None,
    feature_z4_scale: Tuple[float, float] | None = None,
    feature_z4_color: str = 'brown',
    feature_z5: Tuple[str, str] | None = None,
    feature_z5_name: str | None = None,
    feature_z5_agg: Tuple[str, str] | None = None,
    feature_z5_ops: str | None = None,
    feature_z5_scale: Tuple[float, float] | None = None,
    feature_z5_color: str = 'blue',
    feature_z6: Tuple[str, str] | None = None,
    feature_z6_name: str | None = None,
    feature_z6_agg: Tuple[str, str] | None = None,
    feature_z6_ops: str | None = None,
    feature_z6_scale: Tuple[float, float] | None = None,
    feature_z6_color: str = 'red',
    scale_z_axis_on_same_scale: tuple | list = (),
    sort_cols: bool = True,
    show_tables: bool = False,
    interactive: bool = False,
    chart_title: str = 'chart title',
    chart_size_horizontal: float = 10,
    chart_size_vertical: float = 4,
    drift: tuple | list = (1.00, 1.10, 1.20, 1.30, 1.40, 1.50),
) -> None:
    """
    Plots aggregated data with multiple features on several axes and optional customization.

    This function generates a visualization for a DataFrame by grouping and aggregating
    data according to specified features, and allows for multiple additional features to
    be plotted on secondary axes. It supports interactive plotting, customization of chart
    sizes, colors, axis scales, and includes options to display intermediate grouped tables.

    Parameters:
        df_input (DataScienceFrame): The input DataFrame containing data to be processed and plotted.
        columns (list): A list of columns used for grouping data.
        feature_y (str): The y-axis feature column used in the aggregation.
        feature_y_agg (str): The aggregation method for the feature_y column.
        feature_y_name (str): The name to be used for the aggregated y-axis feature.
        feature_z1 (Tuple[str, str] | None): Optional tuple of two columns for the first additional feature.
        feature_z1_name (str | None): Optional name for the first additional feature.
        feature_z1_agg (Tuple[str, str] | None): Aggregation methods for the first feature's columns.
        feature_z1_ops (str | None): Optional operation to perform on the first feature's columns.
        feature_z1_scale (Tuple[float, float] | None): Scale limits for the first feature's axis.
        feature_z1_color (str): Color used for the first feature's axis and plot. Default is 'magenta'.
        feature_z2 (Tuple[str, str] | None): Optional tuple of two columns for the second additional feature.
        feature_z2_name (str | None): Optional name for the second additional feature.
        feature_z2_agg (Tuple[str, str] | None): Aggregation methods for the second feature's columns.
        feature_z2_ops (str | None): Optional operation to perform on the second feature's columns.
        feature_z2_scale (Tuple[float, float] | None): Scale limits for the second feature's axis.
        feature_z2_color (str): Color used for the second feature's axis and plot. Default is 'green'.
        feature_z3 (Tuple[str, str] | None): Optional tuple of two columns for the third additional feature.
        feature_z3_name (str | None): Optional name for the third additional feature.
        feature_z3_agg (Tuple[str, str] | None): Aggregation methods for the third feature's columns.
        feature_z3_ops (str | None): Optional operation to perform on the third feature's columns.
        feature_z3_scale (Tuple[float, float] | None): Scale limits for the third feature's axis.
        feature_z3_color (str): Color used for the third feature's axis and plot. Default is 'lime'.
        feature_z4 (Tuple[str, str] | None): Optional tuple of two columns for the fourth additional feature.
        feature_z4_name (str | None): Optional name for the fourth additional feature.
        feature_z4_agg (Tuple[str, str] | None): Aggregation methods for the fourth feature's columns.
        feature_z4_ops (str | None): Optional operation to perform on the fourth feature's columns.
        feature_z4_scale (Tuple[float, float] | None): Scale limits for the fourth feature's axis.
        feature_z4_color (str): Color used for the fourth feature's axis and plot. Default is 'brown'.
        feature_z5 (Tuple[str, str] | None): Optional tuple of two columns for the fifth additional feature.
        feature_z5_name (str | None): Optional name for the fifth additional feature.
        feature_z5_agg (Tuple[str, str] | None): Aggregation methods for the fifth feature's columns.
        feature_z5_ops (str | None): Optional operation to perform on the fifth feature's columns.
        feature_z5_scale (Tuple[float, float] | None): Scale limits for the fifth feature's axis.
        feature_z5_color (str): Color used for the fifth feature's axis and plot. Default is 'blue'.
        feature_z6 (Tuple[str, str] | None): Optional tuple of two columns for the sixth additional feature.
        feature_z6_name (str | None): Optional name for the sixth additional feature.
        feature_z6_agg (Tuple[str, str] | None): Aggregation methods for the sixth feature's columns.
        feature_z6_ops (str | None): Optional operation to perform on the sixth feature's columns.
        feature_z6_scale (Tuple[float, float] | None): Scale limits for the sixth feature's axis.
        feature_z6_color (str): Color used for the sixth feature's axis and plot. Default is 'red'.
        scale_z_axis_on_same_scale (tuple | list): List or tuple of feature names for aligning y-axis scales.
        sort_cols (bool): Whether to sort the columns before plotting. Default is True.
        show_tables (bool): Option to display intermediate grouped tables. Default is False.
        interactive (bool): Enable interactive plot mode. Default is False.
        chart_title (str): Title of the chart. Default is 'chart title'.
        chart_size_horizontal (float): Horizontal chart size. Default is 10.
        chart_size_vertical (float): Vertical chart size. Default is 4.
        drift (tuple | list): Tuple or list of drifting factors for positioning secondary axes.
            Default is (1.00, 1.10, 1.20, 1.30, 1.40, 1.50).

    Returns:
        None
    """

    if not interactive:
        ipy.run_line_magic('matplotlib', 'inline')
    else:
        ipy.run_line_magic('matplotlib', 'notebook')

    import matplotlib.pyplot as plt

    plt.rcParams.update({'figure.max_open_warning': 0})

    columns = _plot_data_prep(columns=columns, sort_cols=sort_cols)

    features: tuple[tuple] = cast(tuple, (feature_z1, feature_z2, feature_z3, feature_z4, feature_z5, feature_z6))
    feature_agg: tuple[tuple] = cast(
        tuple,
        (
            feature_z1_agg,
            feature_z2_agg,
            feature_z3_agg,
            feature_z4_agg,
            feature_z5_agg,
            feature_z6_agg,
        ),
    )
    feature_name: tuple[tuple] = cast(
        tuple,
        (
            feature_z1_name,
            feature_z2_name,
            feature_z3_name,
            feature_z4_name,
            feature_z5_name,
            feature_z6_name,
        ),
    )
    feature_ops: tuple[str] = cast(
        tuple,
        (
            feature_z1_ops,
            feature_z2_ops,
            feature_z3_ops,
            feature_z4_ops,
            feature_z5_ops,
            feature_z6_ops,
        ),
    )
    feature_scale: tuple[tuple] = cast(
        tuple,
        (
            feature_z1_scale,
            feature_z2_scale,
            feature_z3_scale,
            feature_z4_scale,
            feature_z5_scale,
            feature_z6_scale,
        ),
    )
    colors: tuple[str] = cast(
        tuple,
        (
            feature_z1_color,
            feature_z2_color,
            feature_z3_color,
            feature_z4_color,
            feature_z5_color,
            feature_z6_color,
        ),
    )

    for idx, feature in enumerate(features):
        if feature is not None:
            if len(feature) != 2:
                raise ValueError('Only 2 columns per z-axis can be aggregated!')
            if feature_agg[idx] is None:
                raise ValueError('Feature aggregator is missing')

    for col in columns:
        df = df_input.copy()

        if is_numeric_dtype(df[col].dtype):
            df.fillna(value={col: -100}, inplace=True)
        elif df[col].dtype.name != 'category':
            df.fillna(value={col: 'n/a'}, inplace=True)

        group_axis = {feature_y_name: (feature_y, feature_y_agg)}

        for idx, feature in enumerate(features):
            if feature is not None:
                group_axis[feature[0] + '_' + feature_agg[idx][0]] = (
                    feature[0],
                    feature_agg[idx][0],
                )
                group_axis[feature[1] + '_' + feature_agg[idx][1]] = (
                    feature[1],
                    feature_agg[idx][1],
                )

        df = df_input.groupby([col], as_index=False).agg(**group_axis)

        for idx, feature in enumerate(features):
            if feature is not None:
                df[feature_name[idx]] = OPERATOR[feature_ops[idx]](
                    df[feature[0] + '_' + feature_agg[idx][0]],
                    df[feature[1] + '_' + feature_agg[idx][1]],
                )

        if show_tables:
            display(df)

        fig, ax, x = _plot_exposure(
            feature=col,
            df=df,
            chart_title=chart_title,
            col_exposure=feature_y_name,
            chart_size_horizontal=chart_size_horizontal,
            chart_size_vertical=chart_size_vertical,
            y_label=feature_y_name,
        )

        for feat in range(len(features)):
            if feature_name[feat] is not None:
                if feature_scale[feat] is not None:
                    ymin_sc = feature_scale[feat][0]
                    ymax_sc = feature_scale[feat][1]
                else:
                    if feature_name[feat] in scale_z_axis_on_same_scale:
                        ymin_sc = df[scale_z_axis_on_same_scale].min().min() * 0.9
                        ymax_sc = df[scale_z_axis_on_same_scale].max().max() * 1.1
                    else:
                        feat_min = df[feature_name[feat]].min()
                        if feat_min < 0:
                            fac_min = 1.1
                        else:
                            fac_min = 0.9
                        ymin_sc = feat_min * fac_min

                        feat_max = df[feature_name[feat]].max()
                        if feat_max < 0:
                            fac_max = 0.9
                        else:
                            fac_max = 1.1
                        ymax_sc = feat_max * fac_max

                axis_lbl = feature_name[feat]
                legend_lb = feature_name[feat]

                ax2 = ax.twinx()
                ax2.set_ylim([ymin_sc, ymax_sc])

                ax2.yaxis.set_label_position('right')
                ax2.set_ylabel(axis_lbl, fontsize=12)
                ax2.tick_params(axis='y', colors=colors[feat])
                ax2.yaxis.label.set_color(colors[feat])
                ax2.spines['right'].set_visible(True)

                ax2.spines['right'].set_position(('axes', drift[feat]))

                ax2.plot(
                    x,
                    df[feature_name[feat]],
                    color=colors[feat],
                    marker='o',
                    label=legend_lb,
                )

        fig.legend(
            loc='upper left',
            bbox_to_anchor=(0, 1.02, 1, 0.2),
            ncol=len(features) + 1,
            frameon=False,
            mode='expand',
        )

        plt.show()

        del fig


def _plot_data_prep(columns: Union[str, list], sort_cols: bool = True) -> list:
    """
    Prepare data for plotting by transforming input into a sorted list of columns.

    This function accepts either a string or a list of column names. If a string is provided,
    it will be converted to a list with a single element. It validates that the input is
    either a string or a list and raises a TypeError if the condition is not met. Optionally,
    if 'sort_cols' is set to True, the list of columns will be sorted before being returned.

    Args:
        columns: Union[str, list]
            Either a single column name (as a string) or a list of column names to be processed.
        sort_cols: bool
            Controls whether the resulting list of columns should be sorted.
            Defaults to True.

    Returns:
        list
            A sorted list of column names if 'sort_cols' is True, or an unsorted list
            if 'sort_cols' is False.

    Raises:
        TypeError:
            If the input 'columns' is neither a string nor a list.
    """
    if isinstance(columns, str):
        columns = list(columns)

    if not isinstance(columns, list):
        raise TypeError('Columns to be plotted should be either list or string type!')

    if sort_cols:
        columns.sort()

    return columns


def _plot_exposure(
    feature: str,
    df: DataScienceFrame,
    chart_title: str,
    col_exposure: str,
    chart_size_horizontal: float = 10,
    chart_size_vertical: float = 4,
    y_label: str = 'Exposure',
    exposure_grouping_in_method: bool = True,
) -> Tuple[Any, Any, Any]:
    """
    Plots an exposure chart for a given feature using the provided data frame and parameters.

    This function generates a bar plot to visualize exposure values for unique feature groups
    present in the dataset. It supports customizable chart titles, axis labels, and figure sizes.
    Additionally, it can group exposure-related data depending on the specified `exposure_grouping_in_method` flag.

    Args:
        feature (str): The feature/column name in the dataset used for grouping and plotting.
        df (DataScienceFrame): The data frame containing the data to be plotted.
        chart_title (str): The title displayed on the chart.
        col_exposure (str): The column in the data frame denoting the exposure values.
        chart_size_horizontal (float): The horizontal size of the generated chart in inches. Defaults to 10.
        chart_size_vertical (float): The vertical size of the generated chart in inches. Defaults to 4.
        y_label (str): Label for the Y-axis of the chart. Defaults to 'Exposure'.
        exposure_grouping_in_method (bool): Determines whether to group exposure values by the feature.
            Default is True.

    Returns:
        Tuple[Any, Any, Any]: A tuple containing the matplotlib figure, axes, and x-axis values used in the plot.
    """
    logger.info(f'feature: {feature}')

    x_lab = np.asarray(df[feature].unique()).tolist()
    x_lab.sort()
    x = np.arange(0, df[feature].nunique(), 1)

    titles = feature

    fig, ax = plt.subplots(figsize=(chart_size_horizontal, chart_size_vertical))
    color = 'yellow'
    ax.set_xticks(x)
    ax.set_xticklabels(x_lab, rotation='vertical', fontsize=8)
    ax.set_ylabel(y_label, color='black', fontsize=12)

    if exposure_grouping_in_method:
        exposure = df.groupby(feature)[col_exposure].sum()
    else:
        exposure = df[col_exposure]
        exposure.index = df[feature]

    exposure.plot.bar(
        color=color,
        title=chart_title + ' for ' + titles,
        width=0.5,
        align='center',
        label='Exposure',
    )

    return fig, ax, x


def check_interaction(df_modeling: DataScienceFrame, cols: tuple) -> None:
    """
    Check interaction between specified columns in the modeling DataFrame and visualize
    related metrics such as burning cost, claim frequency, and average claim amount.
    This function segregates data into specified groups, calculates key metrics, and plots
    these metrics as bar charts for easy interpretation.

    Arguments:
        df_modeling (DataScienceFrame): The DataFrame containing modeling data to be
            analyzed. Expected to have the necessary columns for the group-by and computations.
        cols (tuple): A tuple of column names from the DataFrame to group by while performing
            the aggregation and metric calculations.

    Returns:
        None
    """
    group_axis = {
        Modeling.exposure: (Modeling.exposure, 'sum'),
        Modeling.claim_amount: (Modeling.claim_amount, 'sum'),
        Modeling.claim_nb: (Modeling.claim_nb, 'sum'),
    }

    df_grouped = df_modeling.groupby(cols).agg(**group_axis)

    df_grouped[Modeling.burning_cost] = df_grouped[Modeling.claim_amount] / df_grouped[Modeling.exposure]
    df_grouped[Modeling.claim_frequency] = df_grouped[Modeling.claim_nb] / df_grouped[Modeling.exposure]
    df_grouped[Modeling.avg_claim_amount] = df_grouped[Modeling.claim_amount] / df_grouped[Modeling.claim_nb]

    display(df_grouped)

    df_grouped[Modeling.burning_cost].plot(kind='bar', color='yellow', title=Modeling.burning_cost)
    plt.show()

    df_grouped[Modeling.claim_frequency].plot(kind='bar', color='yellow', title=Modeling.claim_frequency)
    plt.show()

    df_grouped[Modeling.avg_claim_amount].plot(kind='bar', color='yellow', title=Modeling.avg_claim_amount)


def plot_in_range(df_modeling: DataScienceFrame, col: str, range_cs: tuple) -> None:
    """
    A function to plot a histogram of a specified column within a given range from a DataScienceFrame.

    This function takes a DataScienceFrame object, a specified column name as a string, and a tuple
    representing the range to filter the data. It then creates a histogram plot for the values
    within the specified range for the given column.

    Parameters:
        df_modeling: DataScienceFrame
            The dataframe containing the data to be plotted.
        col: str
            The name of the column to be plotted.
        range_cs: tuple
            A tuple specifying the (lower_bound, upper_bound) range of the values to plot.

    Returns:
        None
    """
    plt.figure(figsize=(20, 10))

    df_modeling.loc[(df_modeling[col] > range_cs[0]) & (df_modeling[col] < range_cs[1])][col].plot(
        kind='hist',
        bins=100,
        title=col,
    )
