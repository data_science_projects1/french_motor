import copy
from typing import Any, Callable, Type

import pandas as pd
from catboost import CatBoostRegressor
from sklearn.ensemble import (
    AdaBoostRegressor,
    BaggingRegressor,
    GradientBoostingRegressor,
    RandomForestRegressor,
)
from sklearn.model_selection import GridSearchCV, KFold, cross_val_score
from xgboost import XGBRegressor

from scripts.data import (
    Frequency,
    Modeling,
    ModelingFeatureEngineering,
    Severity,
    main_plausibility_checks,
    main_schema_validation_frequency,
    main_schema_validation_severity,
)
from scripts.models import (
    EnsembleModel,
    cols,
    ensemble_model,
    main_frequency_model,
    main_severity_model,
    main_tweedie_model,
)
from scripts.util import DataScienceFrame


def main(modeling_feature_engineering_flag: bool = True) -> DataScienceFrame:
    """
    A primary function that executes the main pipeline of the data science process by integrating multiple steps such as
    frequency calculations, severity computations, modeling, plausibility checks, and optional feature engineering. The
    function ensures data schema validations and cohesive integration between various components.

    Args:
        modeling_feature_engineering_flag (bool, optional): A flag indicating whether to execute the
                                                           feature engineering step as part of modeling.
                                                           Defaults to True.

    Returns:
        DataScienceFrame: The final processed data frame after executing all steps. The output integrates
                          the frequency and severity components into a cohesive model-ready frame.
    """

    df_frequency = Frequency.main().pipe(main_schema_validation_frequency)

    df_severity = Severity.main().pipe(main_schema_validation_severity)

    df_modeling = Modeling.main(df_frequency=df_frequency, df_severity=df_severity)

    main_plausibility_checks(df_modeling=df_modeling, df_severity=df_severity, df_frequency=df_frequency)

    if modeling_feature_engineering_flag:
        df_modeling = ModelingFeatureEngineering(df_modeling=df_modeling).main_preprocessing()
    return df_modeling


class BaseModel:
    """
    Represents a base model framework for data handling, modeling, and plotting.

    This class encapsulates operations required for initializing a base model, fetching
    and preprocessing data, and enabling plotting functionalities for various data
    partitions such as training, testing, and full datasets. The framework is designed
    to be extensible, allowing users to register custom plot strategies as required.

    Attributes:
        cols_to_plot: List of column names to be used for plotting the data.
        filter_claims: Boolean value indicating whether claims should be filtered
            before modeling.
        df_modeling: Instance of DataScienceFrame which contains the data fetched
            for modeling.
        model: Instance of the model created by passing the fetched dataset and
            columns to plot.
        plot_strategies: Dictionary of plotting strategies with keys ("train",
            "test", "full"). Each key maps to a callable that executes the respective
            plotting logic.

    Methods:
        register_plot_strategy: Registers a new plot strategy for a specific data type.
        plot_data: Plots the data based on the specified strategy and data type.
        plot_train_data: Convenience method to plot training data.
        plot_test_data: Convenience method to plot testing data.
        plot_full_data: Convenience method to plot full dataset.
        fetch_data: Fetches and optionally preprocesses the dataset.
        _filter_claims: Static method to filter rows where the claim amount is less
            than or equal to zero.
    """

    def __init__(self, model_instance: Callable, filter_claims: bool = False) -> None:
        self.cols_to_plot = cols
        self.filter_claims = filter_claims
        self.df_modeling: DataScienceFrame = self.fetch_data()
        self.model = model_instance(df_modeling=self.df_modeling, cols_to_plot=self.cols_to_plot)
        self.plot_strategies = {
            'train': lambda: self.model.x_train_plot,
            'test': lambda: self.model.x_test_plot,
            'full': lambda: self.model.df_full_plot,
        }

    def register_plot_strategy(self, data_type: str, strategy: Callable) -> None:
        """
        Registers a new plot strategy to extend the supported data types.

        Args:
            data_type (str): The new data type (e.g., 'custom').
            strategy (callable): A function returning the DataFrame to plot.
        """
        self.plot_strategies[data_type] = strategy

    def plot_data(self, data_type: str) -> None:
        """
        Plot model data using the strategy for the given data type.

        Args:
            data_type (str): The type of data to plot (e.g., 'train', 'test').
        """
        strategy = self.plot_strategies.get(data_type)
        if not strategy:
            raise ValueError(f'Unsupported data_type: {data_type}')
        df_to_plot = strategy()
        self.model.plot_results(df_to_plot=df_to_plot, cols=self.cols_to_plot)

    def plot_train_data(self) -> None:
        self.plot_data(data_type='train')

    def plot_test_data(self) -> None:
        self.plot_data(data_type='test')

    def plot_full_data(self) -> None:
        self.plot_data(data_type='full')

    def fetch_data(self) -> DataScienceFrame:
        """Fetch and optionally preprocess the data."""
        df = main()
        if self.filter_claims:
            df = self._filter_claims(df)
        return df

    @staticmethod
    def _filter_claims(df: DataScienceFrame) -> DataScienceFrame:
        """Filter out rows where claim_amount <= 0."""
        df = df.loc[df[Modeling.claim_amount] > 0].copy()
        df.reset_index(drop=True, inplace=True)
        return df


class FrequencyModel(BaseModel):
    def __init__(self) -> None:
        super().__init__(model_instance=main_frequency_model)


class SeverityModel(BaseModel):
    def __init__(self) -> None:
        super().__init__(model_instance=main_severity_model, filter_claims=True)


class TweedieModel(BaseModel):
    def __init__(self) -> None:
        super().__init__(model_instance=main_tweedie_model)


class EnsembleModelComparison:
    RANDOM_SEED: int = 42

    def __init__(self) -> None:
        self.xgboost_model = None
        self.bagging_model = None
        self.adaboost_model = None
        self.random_forest_model = None
        self.gradient_boosting_model = None
        self.catboost_model = None

        self.df_modeling = main()
        self.ensemble_model = ensemble_model
        self.ensemble_model.train_test_split(df_modeling=self.df_modeling)

    def _run_model_pipeline(self, model: Any) -> EnsembleModel:
        """A generic pipeline to train, evaluate, and log results for a given model."""
        model_instance = copy.deepcopy(self.ensemble_model)
        model_instance.fit_model(model=model)
        model_instance.calc_model_scores()
        print(model_instance.model_scores)
        model_instance.add_prediction()
        model_instance.log_observed_vs_predicted()

        return model_instance

    def cat_boost(self) -> None:
        """Run the pipeline for CatBoostRegressor."""
        self.catboost_model = self._run_model_pipeline(
            model=CatBoostRegressor(random_seed=self.RANDOM_SEED, logging_level='Silent'),
        )

    def gradient_boosting_regressor(self) -> None:
        """Run the pipeline for GradientBoostingRegressor."""
        self.gradient_boosting_model = self._run_model_pipeline(
            model=GradientBoostingRegressor(random_state=self.RANDOM_SEED),
        )

    def random_forest_regressor(self) -> None:
        """Run the pipeline for RandomForestRegressor."""
        self.random_forest_model = self._run_model_pipeline(model=RandomForestRegressor(random_state=self.RANDOM_SEED))

    def adaboost_regressor(self) -> None:
        """Run the pipeline for AdaBoostRegressor."""
        self.adaboost_model = self._run_model_pipeline(model=AdaBoostRegressor(random_state=self.RANDOM_SEED))

    def bagging_regressor(self) -> None:
        """Run the pipeline for BaggingRegressor."""
        self.bagging_model = self._run_model_pipeline(model=BaggingRegressor(random_state=self.RANDOM_SEED))

    def xgboost(self) -> None:
        """Run the pipeline for XGBRegressor."""
        self.xgboost_model = self._run_model_pipeline(model=XGBRegressor(random_state=self.RANDOM_SEED))

    def comparison_table(self) -> None:
        """Generate a comparison table for all models."""
        scores = pd.concat(
            [
                self.catboost_model.model_scores,  # type: ignore
                self.gradient_boosting_model.model_scores,  # type: ignore
                self.adaboost_model.model_scores,  # type: ignore
                self.bagging_model.model_scores,  # type: ignore
                self.random_forest_model.model_scores,  # type: ignore
                self.xgboost_model.model_scores,  # type: ignore
            ],
            axis=1,
            sort=True,
            keys=('CatBoost', 'GradBoost', 'Adaboost', 'Bagging', 'RandomForest', 'XGBoost'),
        )

        with pd.option_context('display.expand_frame_repr', False):
            print(scores)

    def k_fold_cross_validation(self) -> None:
        """
        Conducts k-fold cross-validation for a set of ensemble models.

        This method performs k-fold cross-validation using a predefined set of ensemble
        regression models. Each model's performance is evaluated using the negative
        mean squared error as the scoring metric. The results include the mean and
        standard deviation of cross-validation scores for each model.

        Args:
            None

        Raises:
            None

        Returns:
            None
        """
        ensembles = [
            ('CatBoost', CatBoostRegressor()),
            ('XGBoost', XGBRegressor()),
            ('AB', AdaBoostRegressor()),
            ('GBM', GradientBoostingRegressor()),
            ('BR', BaggingRegressor()),
            ('RF', RandomForestRegressor()),
        ]

        results = []
        names = []

        for name, model in ensembles:
            k_fold = KFold(n_splits=5, random_state=0, shuffle=True)

            cv_results = cross_val_score(
                estimator=model,
                X=self.df_modeling[self.ensemble_model.explanatory],
                y=self.df_modeling[self.ensemble_model.response],
                cv=k_fold,
                params={'sample_weight': self.df_modeling[self.ensemble_model.weights]},
                scoring='neg_mean_squared_error',
            )

            results.append(cv_results)

            names.append(name)

            print(f'{name}: {cv_results.mean():f} ({cv_results.std():f})')

    def catboost_grid_search(
        self,
        grid: dict,
        model: Type[CatBoostRegressor],
        **params: Any,
    ) -> EnsembleModel:
        """
        Perform a grid search optimization using CatBoostRegressor.

        This function executes a grid search over specified hyperparameters for the
        provided CatBoostRegressor model class. It uses the training data and
        labels from the `ensemble_model` attribute to evaluate the parameter grid.
        Once the optimal hyperparameters are determined, it creates a new instance
        of the model with the optimal parameters and additional fixed settings,
        and trains this model using a specified internal pipeline.

        Args:
            grid (dict): A dictionary defining the grid of hyperparameter values
            to search over.
            model (Type[CatBoostRegressor]): The class of the CatBoostRegressor
            model to be optimized.
            **params (Any): Additional parameters to initialize the model before
            performing the grid search.

        Returns:
            EnsembleModel: An instance of the trained ensemble model containing
            the optimized CatBoostRegressor.
        """
        model_instance = model(**params)
        grid_search_result = model_instance.grid_search(
            grid,
            X=self.ensemble_model.x_train[self.ensemble_model.explanatory],
            y=self.ensemble_model.y_train[self.ensemble_model.response],
            plot=False,
            verbose=False,
        )

        print(grid_search_result['params'])

        model_opt_param = model(
            random_seed=self.RANDOM_SEED,
            loss_function='RMSE',
            logging_level='Silent',
            depth=grid_search_result['params']['depth'],
            l2_leaf_reg=grid_search_result['params']['l2_leaf_reg'],
            learning_rate=grid_search_result['params']['learning_rate'],
            subsample=0.95,
        )
        model_optimal = self._run_model_pipeline(model=model_opt_param)

        return model_optimal

    def gradient_boost_grid_search_cv(
        self,
        grid: dict,
        model: Type[GradientBoostingRegressor],
        **params: Any,
    ) -> EnsembleModel:
        """
        Perform grid search cross-validation for Gradient Boosting regressor.

        This function conducts a grid search for hyperparameter optimization for a Gradient
        Boosting Regressor model. It evaluates the specified hyperparameter grid using a scoring
        strategy and identifies the best model parameters. The results of the grid search, including
        the best estimator, best score, and best parameters, are printed. The method also reruns the
        training pipeline with the best hyperparameters to return the optimal model.

        Arguments:
            grid (dict): The grid of hyperparameters to be searched for the Gradient Boosting Regressor.
            model (Type[GradientBoostingRegressor]): The Gradient Boosting Regressor class to be used.
            **params (Any): Additional parameters to be passed when initializing the model.

        Returns:
            EnsembleModel: The ensemble model trained with the best-found hyperparameters.
        """
        model_instance = model(**params)

        fs_gbr = GridSearchCV(
            estimator=model_instance,
            param_grid=grid,
            cv=2,
            n_jobs=-1,
            scoring='neg_mean_squared_error',
            verbose=50,
            return_train_score=True,
        )

        fs_gbr.fit(
            ensemble_model.x_train[ensemble_model.explanatory],
            ensemble_model.y_train[ensemble_model.response],
            sample_weight=ensemble_model.y_train[ensemble_model.weights],
        )

        print(' Results from Random Search ')
        print('\n The best estimator across all searched params:\n', fs_gbr.best_estimator_)
        print('\n The best score across all searched params:\n', fs_gbr.best_score_)
        print('\n The best parameters across all searched params:\n', fs_gbr.best_params_)

        model_optimal = model(
            learning_rate=fs_gbr.best_params_['learning_rate'],
            max_depth=fs_gbr.best_params_['max_depth'],
            n_estimators=fs_gbr.best_params_['n_estimators'],
            random_state=self.RANDOM_SEED,
            subsample=fs_gbr.best_params_['subsample'],
        )

        ensemble_optimal = self._run_model_pipeline(model=model_optimal)

        return ensemble_optimal
