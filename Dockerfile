FROM python:3.11-slim

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    gcc \
    curl \
    git \
    --no-install-recommends && apt-get clean

RUN pip install pipenv

WORKDIR /app

COPY Pipfile* /app/

# Install project dependencies using pipenv
RUN pipenv install --system --deploy

# Copy the rest of the app code into the container
COPY . /app/

EXPOSE 8888
EXPOSE 5000
