install:
	echo "Initialize project"
	pip install pipenv
	pipenv install --dev
	pipenv run pre-commit install

update:
	echo "Updating project dependencies"
	pipenv update

jupyter:
	pipenv run jupyter-lab .

pre-commit:
	pipenv run pre-commit run --all-files

pytest:
	pipenv run pytest test

delete:
	echo "Remove virtual environment"
	pipenv --rm

ruff:
	pipenv run ruff format .
	pipenv run ruff check . --fix

build:
	docker-compose down && docker-compose up --build

start:
	docker-compose up
